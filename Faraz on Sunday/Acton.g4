grammar Acton;
acton:	program EOF;

program: actor program | main;

actor: ACTOR IDENTIFIER (EXTENDS IDENTIFIER | ) LPAR NUMBER RPAR LCURLYBRACE actorBody RCURLYBRACE;

main: MAIN LCURLYBRACE mainBody RCURLYBRACE | ;

actorBody: knownActors actorVars msgHandlers;

knownActors: KNOWNACTORS LCURLYBRACE knownActorsBody RCURLYBRACE;

knownActorsBody: IDENTIFIER IDENTIFIER SEMICOLON knownActorsBody | ;

actorVars: ACTORVARS LCURLYBRACE actorVarsBody RCURLYBRACE;

actorVarsBody: declareVar actorVarsBody | ;

msgHandlers: initial initLess | initLess;

initial: MSGHANDLER INITIAL LPAR declareArgs RPAR LCURLYBRACE msgHandlerBody RCURLYBRACE;

initLess: msgHandler initLess | ;

msgHandler: MSGHANDLER IDENTIFIER LPAR declareArgs RPAR LCURLYBRACE msgHandlerBody RCURLYBRACE;

msgHandlerBody: multiStatementInBraces;

declareArgs: TYPE (IDENTIFIER | arrElement) declareArgsFollowing | ;

declareArgsFollowing: COMMA TYPE (IDENTIFIER | arrElement) declareArgsFollowing | ;

mainBody: IDENTIFIER IDENTIFIER LPAR (callKnownActorsArgs | ) RPAR COLON LPAR (callArgs | ) RPAR SEMICOLON mainBody | ;

callKnownActorsArgs: IDENTIFIER COMMA callKnownActorsArgs | IDENTIFIER | ;

callArgs: (expression | NUMBER | STRINGLITERAL | BOOLLITERAL) COMMA callArgs |
		  (expression | NUMBER | STRINGLITERAL | BOOLLITERAL) | ;

multiStatement: LCURLYBRACE multiStatementInBraces RCURLYBRACE | singleStatement;

multiStatementInBraces: singleStatement multiStatementInBraces | LCURLYBRACE multiStatementInBraces RCURLYBRACE | ;

singleStatement: declareVar | assignment | forLoop | ifCondition | callMethod | BREAK SEMICOLON | CONTINUE SEMICOLON |
 				 print | SEMICOLON;

print: PRINT LPAR (expression | BOOLLITERAL | STRINGLITERAL | NUMBER) RPAR SEMICOLON;

declareVar: TYPE IDENTIFIER SEMICOLON | INT IDENTIFIER LBRACKET NUMBER RBRACKET SEMICOLON;

assignment: IDENTIFIER (LBRACKET NUMBER RBRACKET | ) ASSIGN
			(expression | multiCondition | NUMBER | STRINGLITERAL | BOOLLITERAL | tertiary) SEMICOLON;

forLoop: FOR LPAR forInit SEMICOLON multiCondition SEMICOLON forStep RPAR multiStatement;

forInit: IDENTIFIER ASSIGN NUMBER | ;

forStep: IDENTIFIER ASSIGN expression | IDENTIFIER (INCREMENT | DECREMENT) | (INCREMENT | DECREMENT) IDENTIFIER | ;

ifCondition: IF LPAR multiCondition RPAR multiStatement (ELSE multiStatement | );

multiCondition: singleCondition AND multiCondition | singleCondition OR multiCondition | singleCondition;

singleCondition: expression (EQUAL | NOTEQUAL | GREATERTHAN | LESSTHAN) expression |
		   (expression | BOOLLITERAL) (EQUAL | NOTEQUAL) singleCondition |
		   (expression | SENDER | STRINGLITERAL) (EQUAL | NOTEQUAL) (IDENTIFIER | SENDER | STRINGLITERAL) |
		   NOT singleCondition | LPAR singleCondition RPAR | IDENTIFIER | TRUE | FALSE | ;

expression: term | term (PLUS | MINUS) expression;

term: factor | factor (MULTIPLY | DIVIDE | MODULO) term;

factor: primary | (MINUS | PLUS) factor;

primary: LPAR expression RPAR | IDENTIFIER (INCREMENT | DECREMENT) | (INCREMENT | DECREMENT) IDENTIFIER |
		 (SELF | SENDER | IDENTIFIER) DOT IDENTIFIER | IDENTIFIER | NUMBER | BOOLLITERAL | STRINGLITERAL | arrElement;

arrElement: IDENTIFIER LBRACKET NUMBER RBRACKET;

tertiary: multiCondition QUESTION expression COLON expression;

callMethod: (IDENTIFIER | SELF | SENDER) DOT IDENTIFIER LPAR callArgs RPAR SEMICOLON;

/*
    Lexical Rules
*/

MSGHANDLER:	'msghandler';

INITIAL:	'initial';

EXTENDS:	'extends';

ACTORVARS:	'actorvars';

KNOWNACTORS:	'knownactors';

ACTOR:	'actor';

PRINT:	'print';

FOR:	'for';

ELSE:	'else';

IF:	'if';

SENDER:	'sender';

SELF:	'self';

MAIN:	'main';

NUMBER:	(MINUS | PLUS | )([0-9]+);

STRINGLITERAL:	'"' (~["\n])* '"';

BOOLLITERAL:	(TRUE | FALSE);

TYPE:   STRING | BOOLEAN | INT;

STRING:	'string';

BOOLEAN:	'boolean';

INT:	'int';

TRUE:	'true';

FALSE:	'false';

CONTINUE:	'continue';

BREAK:	'break';

COLON:  ':';

SEMICOLON:  ';';

COMMA:  ',';

DOT:	'.';

QUESTION:	'?';

LCURLYBRACE:	'{';

RCURLYBRACE:	'}';

LPAR:	'(';

RPAR:	')';

LBRACKET:	'[';

RBRACKET:	']';

INCREMENT:	'++';

DECREMENT:	'--';

MINUS:	'-';

PLUS:	'+';

MULTIPLY:	'*';

DIVIDE:	'/';

MODULO:	'%';

EQUAL:	'==';

NOTEQUAL:	'!=';

GREATERTHAN:	'>';

LESSTHAN:	'<';

AND:	'&&';

OR:	'||';

NOT:	'!';

ASSIGN:	'=';

IDENTIFIER:	[_a-zA-Z][_a-zA-Z0-9]*;

WHITESPACE:	[ \t\r\n]+ -> skip;

COMMENT:	'//' ~[\r\n]* -> skip;