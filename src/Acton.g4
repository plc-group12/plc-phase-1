grammar Acton;
acton:	program EOF;

program: actor program | main;

actor: ACTOR IDENTIFIER {System.out.print("ActorDec:" + $IDENTIFIER.text);}
 	   (EXTENDS IDENTIFIER {System.out.println("," + $IDENTIFIER.text);} | {System.out.print("\n");})
 	   LPAR NUMBER RPAR LCURLYBRACE actorBody RCURLYBRACE;

main: MAIN LCURLYBRACE mainBody RCURLYBRACE | ;

actorBody: knownActors actorVars msgHandlers;

knownActors: KNOWNACTORS LCURLYBRACE knownActorsBody RCURLYBRACE;

knownActorsBody: IDENTIFIER {System.out.print("KnownActor:" + $IDENTIFIER.text);}
				 IDENTIFIER {System.out.println("," + $IDENTIFIER.text);} SEMICOLON knownActorsBody | ;

actorVars: ACTORVARS LCURLYBRACE actorVarsBody RCURLYBRACE;

actorVarsBody: declareVar actorVarsBody | ;

msgHandlers: initial initLess | initLess;

initial: MSGHANDLER INITIAL {System.out.print("MsgHandlerDec:" + $INITIAL.text);}
		 LPAR declareArgs RPAR LCURLYBRACE msgHandlerBody RCURLYBRACE;

initLess: msgHandler initLess | ;

msgHandler: MSGHANDLER IDENTIFIER {System.out.print("MsgHandlerDec:" + $IDENTIFIER.text);}
			LPAR declareArgs RPAR LCURLYBRACE msgHandlerBody RCURLYBRACE;

msgHandlerBody: multiStatementInBraces;

declareArgs: TYPE (IDENTIFIER {System.out.print("," + $IDENTIFIER.text);} | arrElement) declareArgsFollowing
			| {System.out.print("\n");};

declareArgsFollowing: COMMA TYPE (IDENTIFIER {System.out.print("," + $IDENTIFIER.text);} | arrElement) declareArgsFollowing
					  | {System.out.print("\n");};

mainBody: IDENTIFIER {System.out.print("ActorInstantiation:" + $IDENTIFIER.text);}
		  IDENTIFIER {System.out.print("," + $IDENTIFIER.text);}
		  LPAR (callKnownActorsArgs | {System.out.print("\n");}) RPAR COLON LPAR (callArgs | ) RPAR SEMICOLON mainBody | ;

callKnownActorsArgs: IDENTIFIER {System.out.print("," + $IDENTIFIER.text);} COMMA callKnownActorsArgs |
					 IDENTIFIER {System.out.println("," + $IDENTIFIER.text);} | {System.out.print("\n");};

callArgs: (expression | NUMBER | STRINGLITERAL | BOOLLITERAL) COMMA callArgs |
		  (expression | NUMBER | STRINGLITERAL | BOOLLITERAL) | ;

multiStatement: LCURLYBRACE multiStatementInBraces RCURLYBRACE | singleStatement;

multiStatementInBraces: singleStatement multiStatementInBraces | LCURLYBRACE multiStatementInBraces RCURLYBRACE | ;

singleStatement: declareVar | assignment | forLoop | ifCondition |
				 {System.out.print("MsgHandlerCall:");} callMethod | BREAK SEMICOLON | CONTINUE SEMICOLON |
 				 print | SEMICOLON;

print: PRINT {System.out.println("Built-in:" + $PRINT.text);}
	   LPAR (expression | BOOLLITERAL | STRINGLITERAL | NUMBER) RPAR SEMICOLON;

declareVar: TYPE (IDENTIFIER | arrElement) {System.out.println("VarDec:" + $TYPE.text + "," + $IDENTIFIER.text);}
			SEMICOLON | INT IDENTIFIER LBRACKET NUMBER RBRACKET {System.out.println("VarDec:" + $INT.text + "," + $IDENTIFIER.text);}
			SEMICOLON;

assignment: IDENTIFIER (LBRACKET NUMBER RBRACKET | ) ASSIGN {System.out.println("Operator:" + $ASSIGN.text);}
			(expression | multiCondition | NUMBER | STRINGLITERAL | BOOLLITERAL | tertiary) SEMICOLON;

forLoop: FOR {System.out.println("Loop:" + $FOR.text);}
		 LPAR forInit SEMICOLON multiCondition SEMICOLON forStep RPAR multiStatement;

forInit: IDENTIFIER ASSIGN {System.out.println("Operator:" + $ASSIGN.text);} NUMBER | ;

forStep: IDENTIFIER ASSIGN {System.out.println("Operator:" + $ASSIGN.text);} expression
		 | IDENTIFIER (INCREMENT {System.out.println("Operator:" + $INCREMENT.text);}
		 | DECREMENT {System.out.println("Operator:" + $DECREMENT.text);})
		 | (INCREMENT {System.out.println("Operator:" + $INCREMENT.text);}
		 | DECREMENT {System.out.println("Operator:" + $DECREMENT.text);}) IDENTIFIER | ;

ifCondition: IF {System.out.println("Conditional:" + $IF.text);}
			 LPAR multiCondition RPAR multiStatement (ELSE {System.out.println("Conditional:" + $ELSE.text);}
			 multiStatement | );

multiCondition: singleCondition AND {System.out.println("Operator:" + $AND.text);} multiCondition
				| singleCondition OR {System.out.println("Operator:" + $OR.text);} multiCondition | singleCondition;

singleCondition: expression (EQUAL {System.out.println("Operator:" + $EQUAL.text);}
				| NOTEQUAL {System.out.println("Operator:" + $NOTEQUAL.text);}
				| GREATERTHAN {System.out.println("Operator:" + $GREATERTHAN.text);}
				| LESSTHAN {System.out.println("Operator:" + $LESSTHAN.text);}) expression |
		   		(expression | BOOLLITERAL) (EQUAL {System.out.println("Operator:" + $EQUAL.text);}
		   		| NOTEQUAL {System.out.println("Operator:" + $NOTEQUAL.text);}) singleCondition |
		   		(expression | SENDER | STRINGLITERAL) (EQUAL {System.out.println("Operator:" + $EQUAL.text);}
		   		| NOTEQUAL {System.out.println("Operator:" + $NOTEQUAL.text);}) (IDENTIFIER | SENDER | STRINGLITERAL) |
		   		NOT {System.out.println("Operator:" + $NOT.text);} singleCondition
		   		| LPAR singleCondition RPAR | IDENTIFIER | TRUE | FALSE | ;

expression: term | term (PLUS {System.out.println("Operator:" + $PLUS.text);}
				 | MINUS {System.out.println("Operator:" + $MINUS.text);}) expression;

term: factor | factor (MULTIPLY {System.out.println("Operator:" + $MULTIPLY.text);}
			 | DIVIDE {System.out.println("Operator:" + $DIVIDE.text);}
			 | MODULO {System.out.println("Operator:" + $MODULO.text);}) term;

factor: primary | (MINUS {System.out.println("Operator:" + $MINUS.text);}
				| PLUS {System.out.println("Operator:" + $PLUS.text);}) factor;

primary: LPAR expression RPAR | IDENTIFIER (INCREMENT {System.out.println("Operator:" + $INCREMENT.text);}
		 | DECREMENT {System.out.println("Operator:" + $DECREMENT.text);})
		 | (INCREMENT {System.out.println("Operator:" + $INCREMENT.text);}
		 | DECREMENT {System.out.println("Operator:" + $DECREMENT.text);}) IDENTIFIER |
		 (SELF | SENDER | IDENTIFIER) DOT IDENTIFIER | IDENTIFIER | NUMBER | BOOLLITERAL | STRINGLITERAL | arrElement;

arrElement: IDENTIFIER LBRACKET NUMBER RBRACKET;

tertiary: multiCondition QUESTION {System.out.println("Operator:?:");} expression COLON expression;

callMethod: (IDENTIFIER {System.out.print($IDENTIFIER.text);} | SELF {System.out.print($SELF.text);} |
			SENDER {System.out.print($SENDER.text);}) DOT IDENTIFIER {System.out.println("," + $IDENTIFIER.text);}
			LPAR callArgs RPAR SEMICOLON;

/*
    Lexical Rules
*/

MSGHANDLER:	'msghandler';

INITIAL:	'initial';

EXTENDS:	'extends';

ACTORVARS:	'actorvars';

KNOWNACTORS:	'knownactors';

ACTOR:	'actor';

PRINT:	'print';

FOR:	'for';

ELSE:	'else';

IF:	'if';

SENDER:	'sender';

SELF:	'self';

MAIN:	'main';

NUMBER:	(MINUS | PLUS | )([0-9]+);

STRINGLITERAL:	'"' (~["\n])* '"';

BOOLLITERAL:	(TRUE | FALSE);

TYPE:   STRING | BOOLEAN | INT;

STRING:	'string';

BOOLEAN:	'boolean';

INT:	'int';

TRUE:	'true';

FALSE:	'false';

CONTINUE:	'continue';

BREAK:	'break';

COLON:  ':';

SEMICOLON:  ';';

COMMA:  ',';

DOT:	'.';

QUESTION:	'?';

LCURLYBRACE:	'{';

RCURLYBRACE:	'}';

LPAR:	'(';

RPAR:	')';

LBRACKET:	'[';

RBRACKET:	']';

INCREMENT:	'++';

DECREMENT:	'--';

MINUS:	'-';

PLUS:	'+';

MULTIPLY:	'*';

DIVIDE:	'/';

MODULO:	'%';

EQUAL:	'==';

NOTEQUAL:	'!=';

GREATERTHAN:	'>';

LESSTHAN:	'<';

AND:	'&&';

OR:	'||';

NOT:	'!';

ASSIGN:	'=';

IDENTIFIER:	[_a-zA-Z][_a-zA-Z0-9]*;

WHITESPACE:	[ \t\r\n]+ -> skip;

COMMENT:	'//' ~[\r\n]* -> skip;