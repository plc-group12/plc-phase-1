// Generated from D:/PLC/Acton/src\Acton.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link ActonParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface ActonVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link ActonParser#acton}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitActon(ActonParser.ActonContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(ActonParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#actor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitActor(ActonParser.ActorContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#main}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMain(ActonParser.MainContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#actorBody}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitActorBody(ActonParser.ActorBodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#knownActors}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKnownActors(ActonParser.KnownActorsContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#knownActorsBody}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKnownActorsBody(ActonParser.KnownActorsBodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#actorVars}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitActorVars(ActonParser.ActorVarsContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#actorVarsBody}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitActorVarsBody(ActonParser.ActorVarsBodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#msgHandlers}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMsgHandlers(ActonParser.MsgHandlersContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#initial}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInitial(ActonParser.InitialContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#initLess}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInitLess(ActonParser.InitLessContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#msgHandler}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMsgHandler(ActonParser.MsgHandlerContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#msgHandlerBody}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMsgHandlerBody(ActonParser.MsgHandlerBodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#declareArgs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclareArgs(ActonParser.DeclareArgsContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#declareArgsFollowing}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclareArgsFollowing(ActonParser.DeclareArgsFollowingContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#mainBody}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMainBody(ActonParser.MainBodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#callKnownActorsArgs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCallKnownActorsArgs(ActonParser.CallKnownActorsArgsContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#callArgs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCallArgs(ActonParser.CallArgsContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#multiStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiStatement(ActonParser.MultiStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#multiStatementInBraces}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiStatementInBraces(ActonParser.MultiStatementInBracesContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#singleStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSingleStatement(ActonParser.SingleStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#print}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrint(ActonParser.PrintContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#declareVar}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclareVar(ActonParser.DeclareVarContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#assignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignment(ActonParser.AssignmentContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#forLoop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForLoop(ActonParser.ForLoopContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#forInit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForInit(ActonParser.ForInitContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#forStep}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForStep(ActonParser.ForStepContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#ifCondition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfCondition(ActonParser.IfConditionContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#multiCondition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiCondition(ActonParser.MultiConditionContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#singleCondition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSingleCondition(ActonParser.SingleConditionContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression(ActonParser.ExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTerm(ActonParser.TermContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#factor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFactor(ActonParser.FactorContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#primary}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimary(ActonParser.PrimaryContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#arrElement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrElement(ActonParser.ArrElementContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#tertiary}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTertiary(ActonParser.TertiaryContext ctx);
	/**
	 * Visit a parse tree produced by {@link ActonParser#callMethod}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCallMethod(ActonParser.CallMethodContext ctx);
}