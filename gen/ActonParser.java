// Generated from D:/PLC/Acton/src\Acton.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ActonParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		MSGHANDLER=1, INITIAL=2, EXTENDS=3, ACTORVARS=4, KNOWNACTORS=5, ACTOR=6, 
		PRINT=7, FOR=8, ELSE=9, IF=10, SENDER=11, SELF=12, MAIN=13, NUMBER=14, 
		STRINGLITERAL=15, BOOLLITERAL=16, TYPE=17, STRING=18, BOOLEAN=19, INT=20, 
		TRUE=21, FALSE=22, CONTINUE=23, BREAK=24, COLON=25, SEMICOLON=26, COMMA=27, 
		DOT=28, QUESTION=29, LCURLYBRACE=30, RCURLYBRACE=31, LPAR=32, RPAR=33, 
		LBRACKET=34, RBRACKET=35, INCREMENT=36, DECREMENT=37, MINUS=38, PLUS=39, 
		MULTIPLY=40, DIVIDE=41, MODULO=42, EQUAL=43, NOTEQUAL=44, GREATERTHAN=45, 
		LESSTHAN=46, AND=47, OR=48, NOT=49, ASSIGN=50, IDENTIFIER=51, WHITESPACE=52, 
		COMMENT=53;
	public static final int
		RULE_acton = 0, RULE_program = 1, RULE_actor = 2, RULE_main = 3, RULE_actorBody = 4, 
		RULE_knownActors = 5, RULE_knownActorsBody = 6, RULE_actorVars = 7, RULE_actorVarsBody = 8, 
		RULE_msgHandlers = 9, RULE_initial = 10, RULE_initLess = 11, RULE_msgHandler = 12, 
		RULE_msgHandlerBody = 13, RULE_declareArgs = 14, RULE_declareArgsFollowing = 15, 
		RULE_mainBody = 16, RULE_callKnownActorsArgs = 17, RULE_callArgs = 18, 
		RULE_multiStatement = 19, RULE_multiStatementInBraces = 20, RULE_singleStatement = 21, 
		RULE_print = 22, RULE_declareVar = 23, RULE_assignment = 24, RULE_forLoop = 25, 
		RULE_forInit = 26, RULE_forStep = 27, RULE_ifCondition = 28, RULE_multiCondition = 29, 
		RULE_singleCondition = 30, RULE_expression = 31, RULE_term = 32, RULE_factor = 33, 
		RULE_primary = 34, RULE_arrElement = 35, RULE_tertiary = 36, RULE_callMethod = 37;
	private static String[] makeRuleNames() {
		return new String[] {
			"acton", "program", "actor", "main", "actorBody", "knownActors", "knownActorsBody", 
			"actorVars", "actorVarsBody", "msgHandlers", "initial", "initLess", "msgHandler", 
			"msgHandlerBody", "declareArgs", "declareArgsFollowing", "mainBody", 
			"callKnownActorsArgs", "callArgs", "multiStatement", "multiStatementInBraces", 
			"singleStatement", "print", "declareVar", "assignment", "forLoop", "forInit", 
			"forStep", "ifCondition", "multiCondition", "singleCondition", "expression", 
			"term", "factor", "primary", "arrElement", "tertiary", "callMethod"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'msghandler'", "'initial'", "'extends'", "'actorvars'", "'knownactors'", 
			"'actor'", "'print'", "'for'", "'else'", "'if'", "'sender'", "'self'", 
			"'main'", null, null, null, null, "'string'", "'boolean'", "'int'", "'true'", 
			"'false'", "'continue'", "'break'", "':'", "';'", "','", "'.'", "'?'", 
			"'{'", "'}'", "'('", "')'", "'['", "']'", "'++'", "'--'", "'-'", "'+'", 
			"'*'", "'/'", "'%'", "'=='", "'!='", "'>'", "'<'", "'&&'", "'||'", "'!'", 
			"'='"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "MSGHANDLER", "INITIAL", "EXTENDS", "ACTORVARS", "KNOWNACTORS", 
			"ACTOR", "PRINT", "FOR", "ELSE", "IF", "SENDER", "SELF", "MAIN", "NUMBER", 
			"STRINGLITERAL", "BOOLLITERAL", "TYPE", "STRING", "BOOLEAN", "INT", "TRUE", 
			"FALSE", "CONTINUE", "BREAK", "COLON", "SEMICOLON", "COMMA", "DOT", "QUESTION", 
			"LCURLYBRACE", "RCURLYBRACE", "LPAR", "RPAR", "LBRACKET", "RBRACKET", 
			"INCREMENT", "DECREMENT", "MINUS", "PLUS", "MULTIPLY", "DIVIDE", "MODULO", 
			"EQUAL", "NOTEQUAL", "GREATERTHAN", "LESSTHAN", "AND", "OR", "NOT", "ASSIGN", 
			"IDENTIFIER", "WHITESPACE", "COMMENT"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Acton.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public ActonParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class ActonContext extends ParserRuleContext {
		public ProgramContext program() {
			return getRuleContext(ProgramContext.class,0);
		}
		public TerminalNode EOF() { return getToken(ActonParser.EOF, 0); }
		public ActonContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_acton; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterActon(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitActon(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitActon(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ActonContext acton() throws RecognitionException {
		ActonContext _localctx = new ActonContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_acton);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(76);
			program();
			setState(77);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ProgramContext extends ParserRuleContext {
		public ActorContext actor() {
			return getRuleContext(ActorContext.class,0);
		}
		public ProgramContext program() {
			return getRuleContext(ProgramContext.class,0);
		}
		public MainContext main() {
			return getRuleContext(MainContext.class,0);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterProgram(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitProgram(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitProgram(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_program);
		try {
			setState(83);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ACTOR:
				enterOuterAlt(_localctx, 1);
				{
				setState(79);
				actor();
				setState(80);
				program();
				}
				break;
			case EOF:
			case MAIN:
				enterOuterAlt(_localctx, 2);
				{
				setState(82);
				main();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ActorContext extends ParserRuleContext {
		public Token IDENTIFIER;
		public TerminalNode ACTOR() { return getToken(ActonParser.ACTOR, 0); }
		public List<TerminalNode> IDENTIFIER() { return getTokens(ActonParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(ActonParser.IDENTIFIER, i);
		}
		public TerminalNode LPAR() { return getToken(ActonParser.LPAR, 0); }
		public TerminalNode NUMBER() { return getToken(ActonParser.NUMBER, 0); }
		public TerminalNode RPAR() { return getToken(ActonParser.RPAR, 0); }
		public TerminalNode LCURLYBRACE() { return getToken(ActonParser.LCURLYBRACE, 0); }
		public ActorBodyContext actorBody() {
			return getRuleContext(ActorBodyContext.class,0);
		}
		public TerminalNode RCURLYBRACE() { return getToken(ActonParser.RCURLYBRACE, 0); }
		public TerminalNode EXTENDS() { return getToken(ActonParser.EXTENDS, 0); }
		public ActorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_actor; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterActor(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitActor(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitActor(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ActorContext actor() throws RecognitionException {
		ActorContext _localctx = new ActorContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_actor);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(85);
			match(ACTOR);
			setState(86);
			((ActorContext)_localctx).IDENTIFIER = match(IDENTIFIER);
			System.out.print("ActorDec:" + (((ActorContext)_localctx).IDENTIFIER!=null?((ActorContext)_localctx).IDENTIFIER.getText():null));
			setState(92);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case EXTENDS:
				{
				setState(88);
				match(EXTENDS);
				setState(89);
				((ActorContext)_localctx).IDENTIFIER = match(IDENTIFIER);
				System.out.println("," + (((ActorContext)_localctx).IDENTIFIER!=null?((ActorContext)_localctx).IDENTIFIER.getText():null));
				}
				break;
			case LPAR:
				{
				System.out.print("\n");
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(94);
			match(LPAR);
			setState(95);
			match(NUMBER);
			setState(96);
			match(RPAR);
			setState(97);
			match(LCURLYBRACE);
			setState(98);
			actorBody();
			setState(99);
			match(RCURLYBRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MainContext extends ParserRuleContext {
		public TerminalNode MAIN() { return getToken(ActonParser.MAIN, 0); }
		public TerminalNode LCURLYBRACE() { return getToken(ActonParser.LCURLYBRACE, 0); }
		public MainBodyContext mainBody() {
			return getRuleContext(MainBodyContext.class,0);
		}
		public TerminalNode RCURLYBRACE() { return getToken(ActonParser.RCURLYBRACE, 0); }
		public MainContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_main; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterMain(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitMain(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitMain(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MainContext main() throws RecognitionException {
		MainContext _localctx = new MainContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_main);
		try {
			setState(107);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case MAIN:
				enterOuterAlt(_localctx, 1);
				{
				setState(101);
				match(MAIN);
				setState(102);
				match(LCURLYBRACE);
				setState(103);
				mainBody();
				setState(104);
				match(RCURLYBRACE);
				}
				break;
			case EOF:
				enterOuterAlt(_localctx, 2);
				{
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ActorBodyContext extends ParserRuleContext {
		public KnownActorsContext knownActors() {
			return getRuleContext(KnownActorsContext.class,0);
		}
		public ActorVarsContext actorVars() {
			return getRuleContext(ActorVarsContext.class,0);
		}
		public MsgHandlersContext msgHandlers() {
			return getRuleContext(MsgHandlersContext.class,0);
		}
		public ActorBodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_actorBody; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterActorBody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitActorBody(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitActorBody(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ActorBodyContext actorBody() throws RecognitionException {
		ActorBodyContext _localctx = new ActorBodyContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_actorBody);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(109);
			knownActors();
			setState(110);
			actorVars();
			setState(111);
			msgHandlers();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KnownActorsContext extends ParserRuleContext {
		public TerminalNode KNOWNACTORS() { return getToken(ActonParser.KNOWNACTORS, 0); }
		public TerminalNode LCURLYBRACE() { return getToken(ActonParser.LCURLYBRACE, 0); }
		public KnownActorsBodyContext knownActorsBody() {
			return getRuleContext(KnownActorsBodyContext.class,0);
		}
		public TerminalNode RCURLYBRACE() { return getToken(ActonParser.RCURLYBRACE, 0); }
		public KnownActorsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_knownActors; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterKnownActors(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitKnownActors(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitKnownActors(this);
			else return visitor.visitChildren(this);
		}
	}

	public final KnownActorsContext knownActors() throws RecognitionException {
		KnownActorsContext _localctx = new KnownActorsContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_knownActors);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(113);
			match(KNOWNACTORS);
			setState(114);
			match(LCURLYBRACE);
			setState(115);
			knownActorsBody();
			setState(116);
			match(RCURLYBRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KnownActorsBodyContext extends ParserRuleContext {
		public Token IDENTIFIER;
		public List<TerminalNode> IDENTIFIER() { return getTokens(ActonParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(ActonParser.IDENTIFIER, i);
		}
		public TerminalNode SEMICOLON() { return getToken(ActonParser.SEMICOLON, 0); }
		public KnownActorsBodyContext knownActorsBody() {
			return getRuleContext(KnownActorsBodyContext.class,0);
		}
		public KnownActorsBodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_knownActorsBody; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterKnownActorsBody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitKnownActorsBody(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitKnownActorsBody(this);
			else return visitor.visitChildren(this);
		}
	}

	public final KnownActorsBodyContext knownActorsBody() throws RecognitionException {
		KnownActorsBodyContext _localctx = new KnownActorsBodyContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_knownActorsBody);
		try {
			setState(125);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case IDENTIFIER:
				enterOuterAlt(_localctx, 1);
				{
				setState(118);
				((KnownActorsBodyContext)_localctx).IDENTIFIER = match(IDENTIFIER);
				System.out.print("KnownActor:" + (((KnownActorsBodyContext)_localctx).IDENTIFIER!=null?((KnownActorsBodyContext)_localctx).IDENTIFIER.getText():null));
				setState(120);
				((KnownActorsBodyContext)_localctx).IDENTIFIER = match(IDENTIFIER);
				System.out.println("," + (((KnownActorsBodyContext)_localctx).IDENTIFIER!=null?((KnownActorsBodyContext)_localctx).IDENTIFIER.getText():null));
				setState(122);
				match(SEMICOLON);
				setState(123);
				knownActorsBody();
				}
				break;
			case RCURLYBRACE:
				enterOuterAlt(_localctx, 2);
				{
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ActorVarsContext extends ParserRuleContext {
		public TerminalNode ACTORVARS() { return getToken(ActonParser.ACTORVARS, 0); }
		public TerminalNode LCURLYBRACE() { return getToken(ActonParser.LCURLYBRACE, 0); }
		public ActorVarsBodyContext actorVarsBody() {
			return getRuleContext(ActorVarsBodyContext.class,0);
		}
		public TerminalNode RCURLYBRACE() { return getToken(ActonParser.RCURLYBRACE, 0); }
		public ActorVarsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_actorVars; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterActorVars(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitActorVars(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitActorVars(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ActorVarsContext actorVars() throws RecognitionException {
		ActorVarsContext _localctx = new ActorVarsContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_actorVars);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(127);
			match(ACTORVARS);
			setState(128);
			match(LCURLYBRACE);
			setState(129);
			actorVarsBody();
			setState(130);
			match(RCURLYBRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ActorVarsBodyContext extends ParserRuleContext {
		public DeclareVarContext declareVar() {
			return getRuleContext(DeclareVarContext.class,0);
		}
		public ActorVarsBodyContext actorVarsBody() {
			return getRuleContext(ActorVarsBodyContext.class,0);
		}
		public ActorVarsBodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_actorVarsBody; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterActorVarsBody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitActorVarsBody(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitActorVarsBody(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ActorVarsBodyContext actorVarsBody() throws RecognitionException {
		ActorVarsBodyContext _localctx = new ActorVarsBodyContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_actorVarsBody);
		try {
			setState(136);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case TYPE:
			case INT:
				enterOuterAlt(_localctx, 1);
				{
				setState(132);
				declareVar();
				setState(133);
				actorVarsBody();
				}
				break;
			case RCURLYBRACE:
				enterOuterAlt(_localctx, 2);
				{
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MsgHandlersContext extends ParserRuleContext {
		public InitialContext initial() {
			return getRuleContext(InitialContext.class,0);
		}
		public InitLessContext initLess() {
			return getRuleContext(InitLessContext.class,0);
		}
		public MsgHandlersContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_msgHandlers; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterMsgHandlers(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitMsgHandlers(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitMsgHandlers(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MsgHandlersContext msgHandlers() throws RecognitionException {
		MsgHandlersContext _localctx = new MsgHandlersContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_msgHandlers);
		try {
			setState(142);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,5,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(138);
				initial();
				setState(139);
				initLess();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(141);
				initLess();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InitialContext extends ParserRuleContext {
		public Token INITIAL;
		public TerminalNode MSGHANDLER() { return getToken(ActonParser.MSGHANDLER, 0); }
		public TerminalNode INITIAL() { return getToken(ActonParser.INITIAL, 0); }
		public TerminalNode LPAR() { return getToken(ActonParser.LPAR, 0); }
		public DeclareArgsContext declareArgs() {
			return getRuleContext(DeclareArgsContext.class,0);
		}
		public TerminalNode RPAR() { return getToken(ActonParser.RPAR, 0); }
		public TerminalNode LCURLYBRACE() { return getToken(ActonParser.LCURLYBRACE, 0); }
		public MsgHandlerBodyContext msgHandlerBody() {
			return getRuleContext(MsgHandlerBodyContext.class,0);
		}
		public TerminalNode RCURLYBRACE() { return getToken(ActonParser.RCURLYBRACE, 0); }
		public InitialContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_initial; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterInitial(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitInitial(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitInitial(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InitialContext initial() throws RecognitionException {
		InitialContext _localctx = new InitialContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_initial);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(144);
			match(MSGHANDLER);
			setState(145);
			((InitialContext)_localctx).INITIAL = match(INITIAL);
			System.out.print("MsgHandlerDec:" + (((InitialContext)_localctx).INITIAL!=null?((InitialContext)_localctx).INITIAL.getText():null));
			setState(147);
			match(LPAR);
			setState(148);
			declareArgs();
			setState(149);
			match(RPAR);
			setState(150);
			match(LCURLYBRACE);
			setState(151);
			msgHandlerBody();
			setState(152);
			match(RCURLYBRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InitLessContext extends ParserRuleContext {
		public MsgHandlerContext msgHandler() {
			return getRuleContext(MsgHandlerContext.class,0);
		}
		public InitLessContext initLess() {
			return getRuleContext(InitLessContext.class,0);
		}
		public InitLessContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_initLess; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterInitLess(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitInitLess(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitInitLess(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InitLessContext initLess() throws RecognitionException {
		InitLessContext _localctx = new InitLessContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_initLess);
		try {
			setState(158);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case MSGHANDLER:
				enterOuterAlt(_localctx, 1);
				{
				setState(154);
				msgHandler();
				setState(155);
				initLess();
				}
				break;
			case RCURLYBRACE:
				enterOuterAlt(_localctx, 2);
				{
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MsgHandlerContext extends ParserRuleContext {
		public Token IDENTIFIER;
		public TerminalNode MSGHANDLER() { return getToken(ActonParser.MSGHANDLER, 0); }
		public TerminalNode IDENTIFIER() { return getToken(ActonParser.IDENTIFIER, 0); }
		public TerminalNode LPAR() { return getToken(ActonParser.LPAR, 0); }
		public DeclareArgsContext declareArgs() {
			return getRuleContext(DeclareArgsContext.class,0);
		}
		public TerminalNode RPAR() { return getToken(ActonParser.RPAR, 0); }
		public TerminalNode LCURLYBRACE() { return getToken(ActonParser.LCURLYBRACE, 0); }
		public MsgHandlerBodyContext msgHandlerBody() {
			return getRuleContext(MsgHandlerBodyContext.class,0);
		}
		public TerminalNode RCURLYBRACE() { return getToken(ActonParser.RCURLYBRACE, 0); }
		public MsgHandlerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_msgHandler; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterMsgHandler(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitMsgHandler(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitMsgHandler(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MsgHandlerContext msgHandler() throws RecognitionException {
		MsgHandlerContext _localctx = new MsgHandlerContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_msgHandler);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(160);
			match(MSGHANDLER);
			setState(161);
			((MsgHandlerContext)_localctx).IDENTIFIER = match(IDENTIFIER);
			System.out.print("MsgHandlerDec:" + (((MsgHandlerContext)_localctx).IDENTIFIER!=null?((MsgHandlerContext)_localctx).IDENTIFIER.getText():null));
			setState(163);
			match(LPAR);
			setState(164);
			declareArgs();
			setState(165);
			match(RPAR);
			setState(166);
			match(LCURLYBRACE);
			setState(167);
			msgHandlerBody();
			setState(168);
			match(RCURLYBRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MsgHandlerBodyContext extends ParserRuleContext {
		public MultiStatementInBracesContext multiStatementInBraces() {
			return getRuleContext(MultiStatementInBracesContext.class,0);
		}
		public MsgHandlerBodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_msgHandlerBody; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterMsgHandlerBody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitMsgHandlerBody(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitMsgHandlerBody(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MsgHandlerBodyContext msgHandlerBody() throws RecognitionException {
		MsgHandlerBodyContext _localctx = new MsgHandlerBodyContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_msgHandlerBody);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(170);
			multiStatementInBraces();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclareArgsContext extends ParserRuleContext {
		public Token IDENTIFIER;
		public TerminalNode TYPE() { return getToken(ActonParser.TYPE, 0); }
		public DeclareArgsFollowingContext declareArgsFollowing() {
			return getRuleContext(DeclareArgsFollowingContext.class,0);
		}
		public TerminalNode IDENTIFIER() { return getToken(ActonParser.IDENTIFIER, 0); }
		public ArrElementContext arrElement() {
			return getRuleContext(ArrElementContext.class,0);
		}
		public DeclareArgsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declareArgs; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterDeclareArgs(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitDeclareArgs(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitDeclareArgs(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DeclareArgsContext declareArgs() throws RecognitionException {
		DeclareArgsContext _localctx = new DeclareArgsContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_declareArgs);
		try {
			setState(180);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case TYPE:
				enterOuterAlt(_localctx, 1);
				{
				setState(172);
				match(TYPE);
				setState(176);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
				case 1:
					{
					setState(173);
					((DeclareArgsContext)_localctx).IDENTIFIER = match(IDENTIFIER);
					System.out.print("," + (((DeclareArgsContext)_localctx).IDENTIFIER!=null?((DeclareArgsContext)_localctx).IDENTIFIER.getText():null));
					}
					break;
				case 2:
					{
					setState(175);
					arrElement();
					}
					break;
				}
				setState(178);
				declareArgsFollowing();
				}
				break;
			case RPAR:
				enterOuterAlt(_localctx, 2);
				{
				System.out.print("\n");
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclareArgsFollowingContext extends ParserRuleContext {
		public Token IDENTIFIER;
		public TerminalNode COMMA() { return getToken(ActonParser.COMMA, 0); }
		public TerminalNode TYPE() { return getToken(ActonParser.TYPE, 0); }
		public DeclareArgsFollowingContext declareArgsFollowing() {
			return getRuleContext(DeclareArgsFollowingContext.class,0);
		}
		public TerminalNode IDENTIFIER() { return getToken(ActonParser.IDENTIFIER, 0); }
		public ArrElementContext arrElement() {
			return getRuleContext(ArrElementContext.class,0);
		}
		public DeclareArgsFollowingContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declareArgsFollowing; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterDeclareArgsFollowing(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitDeclareArgsFollowing(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitDeclareArgsFollowing(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DeclareArgsFollowingContext declareArgsFollowing() throws RecognitionException {
		DeclareArgsFollowingContext _localctx = new DeclareArgsFollowingContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_declareArgsFollowing);
		try {
			setState(191);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case COMMA:
				enterOuterAlt(_localctx, 1);
				{
				setState(182);
				match(COMMA);
				setState(183);
				match(TYPE);
				setState(187);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
				case 1:
					{
					setState(184);
					((DeclareArgsFollowingContext)_localctx).IDENTIFIER = match(IDENTIFIER);
					System.out.print("," + (((DeclareArgsFollowingContext)_localctx).IDENTIFIER!=null?((DeclareArgsFollowingContext)_localctx).IDENTIFIER.getText():null));
					}
					break;
				case 2:
					{
					setState(186);
					arrElement();
					}
					break;
				}
				setState(189);
				declareArgsFollowing();
				}
				break;
			case RPAR:
				enterOuterAlt(_localctx, 2);
				{
				System.out.print("\n");
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MainBodyContext extends ParserRuleContext {
		public Token IDENTIFIER;
		public List<TerminalNode> IDENTIFIER() { return getTokens(ActonParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(ActonParser.IDENTIFIER, i);
		}
		public List<TerminalNode> LPAR() { return getTokens(ActonParser.LPAR); }
		public TerminalNode LPAR(int i) {
			return getToken(ActonParser.LPAR, i);
		}
		public List<TerminalNode> RPAR() { return getTokens(ActonParser.RPAR); }
		public TerminalNode RPAR(int i) {
			return getToken(ActonParser.RPAR, i);
		}
		public TerminalNode COLON() { return getToken(ActonParser.COLON, 0); }
		public TerminalNode SEMICOLON() { return getToken(ActonParser.SEMICOLON, 0); }
		public MainBodyContext mainBody() {
			return getRuleContext(MainBodyContext.class,0);
		}
		public CallKnownActorsArgsContext callKnownActorsArgs() {
			return getRuleContext(CallKnownActorsArgsContext.class,0);
		}
		public CallArgsContext callArgs() {
			return getRuleContext(CallArgsContext.class,0);
		}
		public MainBodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mainBody; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterMainBody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitMainBody(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitMainBody(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MainBodyContext mainBody() throws RecognitionException {
		MainBodyContext _localctx = new MainBodyContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_mainBody);
		try {
			setState(213);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case IDENTIFIER:
				enterOuterAlt(_localctx, 1);
				{
				setState(193);
				((MainBodyContext)_localctx).IDENTIFIER = match(IDENTIFIER);
				System.out.print("ActorInstantiation:" + (((MainBodyContext)_localctx).IDENTIFIER!=null?((MainBodyContext)_localctx).IDENTIFIER.getText():null));
				setState(195);
				((MainBodyContext)_localctx).IDENTIFIER = match(IDENTIFIER);
				System.out.print("," + (((MainBodyContext)_localctx).IDENTIFIER!=null?((MainBodyContext)_localctx).IDENTIFIER.getText():null));
				setState(197);
				match(LPAR);
				setState(200);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
				case 1:
					{
					setState(198);
					callKnownActorsArgs();
					}
					break;
				case 2:
					{
					System.out.print("\n");
					}
					break;
				}
				setState(202);
				match(RPAR);
				setState(203);
				match(COLON);
				setState(204);
				match(LPAR);
				setState(207);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
				case 1:
					{
					setState(205);
					callArgs();
					}
					break;
				case 2:
					{
					}
					break;
				}
				setState(209);
				match(RPAR);
				setState(210);
				match(SEMICOLON);
				setState(211);
				mainBody();
				}
				break;
			case RCURLYBRACE:
				enterOuterAlt(_localctx, 2);
				{
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CallKnownActorsArgsContext extends ParserRuleContext {
		public Token IDENTIFIER;
		public TerminalNode IDENTIFIER() { return getToken(ActonParser.IDENTIFIER, 0); }
		public TerminalNode COMMA() { return getToken(ActonParser.COMMA, 0); }
		public CallKnownActorsArgsContext callKnownActorsArgs() {
			return getRuleContext(CallKnownActorsArgsContext.class,0);
		}
		public CallKnownActorsArgsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_callKnownActorsArgs; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterCallKnownActorsArgs(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitCallKnownActorsArgs(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitCallKnownActorsArgs(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CallKnownActorsArgsContext callKnownActorsArgs() throws RecognitionException {
		CallKnownActorsArgsContext _localctx = new CallKnownActorsArgsContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_callKnownActorsArgs);
		try {
			setState(222);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(215);
				((CallKnownActorsArgsContext)_localctx).IDENTIFIER = match(IDENTIFIER);
				System.out.print("," + (((CallKnownActorsArgsContext)_localctx).IDENTIFIER!=null?((CallKnownActorsArgsContext)_localctx).IDENTIFIER.getText():null));
				setState(217);
				match(COMMA);
				setState(218);
				callKnownActorsArgs();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(219);
				((CallKnownActorsArgsContext)_localctx).IDENTIFIER = match(IDENTIFIER);
				System.out.println("," + (((CallKnownActorsArgsContext)_localctx).IDENTIFIER!=null?((CallKnownActorsArgsContext)_localctx).IDENTIFIER.getText():null));
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				System.out.print("\n");
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CallArgsContext extends ParserRuleContext {
		public TerminalNode COMMA() { return getToken(ActonParser.COMMA, 0); }
		public CallArgsContext callArgs() {
			return getRuleContext(CallArgsContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode NUMBER() { return getToken(ActonParser.NUMBER, 0); }
		public TerminalNode STRINGLITERAL() { return getToken(ActonParser.STRINGLITERAL, 0); }
		public TerminalNode BOOLLITERAL() { return getToken(ActonParser.BOOLLITERAL, 0); }
		public CallArgsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_callArgs; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterCallArgs(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitCallArgs(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitCallArgs(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CallArgsContext callArgs() throws RecognitionException {
		CallArgsContext _localctx = new CallArgsContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_callArgs);
		try {
			setState(239);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,17,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(228);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
				case 1:
					{
					setState(224);
					expression();
					}
					break;
				case 2:
					{
					setState(225);
					match(NUMBER);
					}
					break;
				case 3:
					{
					setState(226);
					match(STRINGLITERAL);
					}
					break;
				case 4:
					{
					setState(227);
					match(BOOLLITERAL);
					}
					break;
				}
				setState(230);
				match(COMMA);
				setState(231);
				callArgs();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(236);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,16,_ctx) ) {
				case 1:
					{
					setState(232);
					expression();
					}
					break;
				case 2:
					{
					setState(233);
					match(NUMBER);
					}
					break;
				case 3:
					{
					setState(234);
					match(STRINGLITERAL);
					}
					break;
				case 4:
					{
					setState(235);
					match(BOOLLITERAL);
					}
					break;
				}
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MultiStatementContext extends ParserRuleContext {
		public TerminalNode LCURLYBRACE() { return getToken(ActonParser.LCURLYBRACE, 0); }
		public MultiStatementInBracesContext multiStatementInBraces() {
			return getRuleContext(MultiStatementInBracesContext.class,0);
		}
		public TerminalNode RCURLYBRACE() { return getToken(ActonParser.RCURLYBRACE, 0); }
		public SingleStatementContext singleStatement() {
			return getRuleContext(SingleStatementContext.class,0);
		}
		public MultiStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multiStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterMultiStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitMultiStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitMultiStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MultiStatementContext multiStatement() throws RecognitionException {
		MultiStatementContext _localctx = new MultiStatementContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_multiStatement);
		try {
			setState(246);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case LCURLYBRACE:
				enterOuterAlt(_localctx, 1);
				{
				setState(241);
				match(LCURLYBRACE);
				setState(242);
				multiStatementInBraces();
				setState(243);
				match(RCURLYBRACE);
				}
				break;
			case PRINT:
			case FOR:
			case IF:
			case SENDER:
			case SELF:
			case TYPE:
			case INT:
			case CONTINUE:
			case BREAK:
			case SEMICOLON:
			case IDENTIFIER:
				enterOuterAlt(_localctx, 2);
				{
				setState(245);
				singleStatement();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MultiStatementInBracesContext extends ParserRuleContext {
		public SingleStatementContext singleStatement() {
			return getRuleContext(SingleStatementContext.class,0);
		}
		public MultiStatementInBracesContext multiStatementInBraces() {
			return getRuleContext(MultiStatementInBracesContext.class,0);
		}
		public TerminalNode LCURLYBRACE() { return getToken(ActonParser.LCURLYBRACE, 0); }
		public TerminalNode RCURLYBRACE() { return getToken(ActonParser.RCURLYBRACE, 0); }
		public MultiStatementInBracesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multiStatementInBraces; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterMultiStatementInBraces(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitMultiStatementInBraces(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitMultiStatementInBraces(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MultiStatementInBracesContext multiStatementInBraces() throws RecognitionException {
		MultiStatementInBracesContext _localctx = new MultiStatementInBracesContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_multiStatementInBraces);
		try {
			setState(256);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case PRINT:
			case FOR:
			case IF:
			case SENDER:
			case SELF:
			case TYPE:
			case INT:
			case CONTINUE:
			case BREAK:
			case SEMICOLON:
			case IDENTIFIER:
				enterOuterAlt(_localctx, 1);
				{
				setState(248);
				singleStatement();
				setState(249);
				multiStatementInBraces();
				}
				break;
			case LCURLYBRACE:
				enterOuterAlt(_localctx, 2);
				{
				setState(251);
				match(LCURLYBRACE);
				setState(252);
				multiStatementInBraces();
				setState(253);
				match(RCURLYBRACE);
				}
				break;
			case RCURLYBRACE:
				enterOuterAlt(_localctx, 3);
				{
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SingleStatementContext extends ParserRuleContext {
		public DeclareVarContext declareVar() {
			return getRuleContext(DeclareVarContext.class,0);
		}
		public AssignmentContext assignment() {
			return getRuleContext(AssignmentContext.class,0);
		}
		public ForLoopContext forLoop() {
			return getRuleContext(ForLoopContext.class,0);
		}
		public IfConditionContext ifCondition() {
			return getRuleContext(IfConditionContext.class,0);
		}
		public CallMethodContext callMethod() {
			return getRuleContext(CallMethodContext.class,0);
		}
		public TerminalNode BREAK() { return getToken(ActonParser.BREAK, 0); }
		public TerminalNode SEMICOLON() { return getToken(ActonParser.SEMICOLON, 0); }
		public TerminalNode CONTINUE() { return getToken(ActonParser.CONTINUE, 0); }
		public PrintContext print() {
			return getRuleContext(PrintContext.class,0);
		}
		public SingleStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_singleStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterSingleStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitSingleStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitSingleStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SingleStatementContext singleStatement() throws RecognitionException {
		SingleStatementContext _localctx = new SingleStatementContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_singleStatement);
		try {
			setState(270);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,20,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(258);
				declareVar();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(259);
				assignment();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(260);
				forLoop();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(261);
				ifCondition();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				System.out.print("MsgHandlerCall:");
				setState(263);
				callMethod();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(264);
				match(BREAK);
				setState(265);
				match(SEMICOLON);
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(266);
				match(CONTINUE);
				setState(267);
				match(SEMICOLON);
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(268);
				print();
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(269);
				match(SEMICOLON);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrintContext extends ParserRuleContext {
		public Token PRINT;
		public TerminalNode PRINT() { return getToken(ActonParser.PRINT, 0); }
		public TerminalNode LPAR() { return getToken(ActonParser.LPAR, 0); }
		public TerminalNode RPAR() { return getToken(ActonParser.RPAR, 0); }
		public TerminalNode SEMICOLON() { return getToken(ActonParser.SEMICOLON, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode BOOLLITERAL() { return getToken(ActonParser.BOOLLITERAL, 0); }
		public TerminalNode STRINGLITERAL() { return getToken(ActonParser.STRINGLITERAL, 0); }
		public TerminalNode NUMBER() { return getToken(ActonParser.NUMBER, 0); }
		public PrintContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_print; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterPrint(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitPrint(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitPrint(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PrintContext print() throws RecognitionException {
		PrintContext _localctx = new PrintContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_print);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(272);
			((PrintContext)_localctx).PRINT = match(PRINT);
			System.out.println("Built-in:" + (((PrintContext)_localctx).PRINT!=null?((PrintContext)_localctx).PRINT.getText():null));
			setState(274);
			match(LPAR);
			setState(279);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,21,_ctx) ) {
			case 1:
				{
				setState(275);
				expression();
				}
				break;
			case 2:
				{
				setState(276);
				match(BOOLLITERAL);
				}
				break;
			case 3:
				{
				setState(277);
				match(STRINGLITERAL);
				}
				break;
			case 4:
				{
				setState(278);
				match(NUMBER);
				}
				break;
			}
			setState(281);
			match(RPAR);
			setState(282);
			match(SEMICOLON);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclareVarContext extends ParserRuleContext {
		public Token TYPE;
		public Token IDENTIFIER;
		public Token INT;
		public TerminalNode TYPE() { return getToken(ActonParser.TYPE, 0); }
		public TerminalNode IDENTIFIER() { return getToken(ActonParser.IDENTIFIER, 0); }
		public TerminalNode SEMICOLON() { return getToken(ActonParser.SEMICOLON, 0); }
		public TerminalNode INT() { return getToken(ActonParser.INT, 0); }
		public TerminalNode LBRACKET() { return getToken(ActonParser.LBRACKET, 0); }
		public TerminalNode NUMBER() { return getToken(ActonParser.NUMBER, 0); }
		public TerminalNode RBRACKET() { return getToken(ActonParser.RBRACKET, 0); }
		public DeclareVarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declareVar; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterDeclareVar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitDeclareVar(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitDeclareVar(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DeclareVarContext declareVar() throws RecognitionException {
		DeclareVarContext _localctx = new DeclareVarContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_declareVar);
		try {
			setState(295);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case TYPE:
				enterOuterAlt(_localctx, 1);
				{
				setState(284);
				((DeclareVarContext)_localctx).TYPE = match(TYPE);
				setState(285);
				((DeclareVarContext)_localctx).IDENTIFIER = match(IDENTIFIER);
				System.out.println("VarDec:" + (((DeclareVarContext)_localctx).TYPE!=null?((DeclareVarContext)_localctx).TYPE.getText():null) + "," + (((DeclareVarContext)_localctx).IDENTIFIER!=null?((DeclareVarContext)_localctx).IDENTIFIER.getText():null));
				setState(287);
				match(SEMICOLON);
				}
				break;
			case INT:
				enterOuterAlt(_localctx, 2);
				{
				setState(288);
				((DeclareVarContext)_localctx).INT = match(INT);
				setState(289);
				((DeclareVarContext)_localctx).IDENTIFIER = match(IDENTIFIER);
				setState(290);
				match(LBRACKET);
				setState(291);
				match(NUMBER);
				setState(292);
				match(RBRACKET);
				System.out.println("VarDec:" + (((DeclareVarContext)_localctx).INT!=null?((DeclareVarContext)_localctx).INT.getText():null) + "," + (((DeclareVarContext)_localctx).IDENTIFIER!=null?((DeclareVarContext)_localctx).IDENTIFIER.getText():null));
				setState(294);
				match(SEMICOLON);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignmentContext extends ParserRuleContext {
		public Token ASSIGN;
		public TerminalNode IDENTIFIER() { return getToken(ActonParser.IDENTIFIER, 0); }
		public TerminalNode ASSIGN() { return getToken(ActonParser.ASSIGN, 0); }
		public TerminalNode SEMICOLON() { return getToken(ActonParser.SEMICOLON, 0); }
		public TerminalNode LBRACKET() { return getToken(ActonParser.LBRACKET, 0); }
		public List<TerminalNode> NUMBER() { return getTokens(ActonParser.NUMBER); }
		public TerminalNode NUMBER(int i) {
			return getToken(ActonParser.NUMBER, i);
		}
		public TerminalNode RBRACKET() { return getToken(ActonParser.RBRACKET, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public MultiConditionContext multiCondition() {
			return getRuleContext(MultiConditionContext.class,0);
		}
		public TerminalNode STRINGLITERAL() { return getToken(ActonParser.STRINGLITERAL, 0); }
		public TerminalNode BOOLLITERAL() { return getToken(ActonParser.BOOLLITERAL, 0); }
		public TertiaryContext tertiary() {
			return getRuleContext(TertiaryContext.class,0);
		}
		public AssignmentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignment; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterAssignment(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitAssignment(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitAssignment(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AssignmentContext assignment() throws RecognitionException {
		AssignmentContext _localctx = new AssignmentContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_assignment);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(297);
			match(IDENTIFIER);
			setState(302);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case LBRACKET:
				{
				setState(298);
				match(LBRACKET);
				setState(299);
				match(NUMBER);
				setState(300);
				match(RBRACKET);
				}
				break;
			case ASSIGN:
				{
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(304);
			((AssignmentContext)_localctx).ASSIGN = match(ASSIGN);
			System.out.println("Operator:" + (((AssignmentContext)_localctx).ASSIGN!=null?((AssignmentContext)_localctx).ASSIGN.getText():null));
			setState(312);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,24,_ctx) ) {
			case 1:
				{
				setState(306);
				expression();
				}
				break;
			case 2:
				{
				setState(307);
				multiCondition();
				}
				break;
			case 3:
				{
				setState(308);
				match(NUMBER);
				}
				break;
			case 4:
				{
				setState(309);
				match(STRINGLITERAL);
				}
				break;
			case 5:
				{
				setState(310);
				match(BOOLLITERAL);
				}
				break;
			case 6:
				{
				setState(311);
				tertiary();
				}
				break;
			}
			setState(314);
			match(SEMICOLON);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ForLoopContext extends ParserRuleContext {
		public Token FOR;
		public TerminalNode FOR() { return getToken(ActonParser.FOR, 0); }
		public TerminalNode LPAR() { return getToken(ActonParser.LPAR, 0); }
		public ForInitContext forInit() {
			return getRuleContext(ForInitContext.class,0);
		}
		public List<TerminalNode> SEMICOLON() { return getTokens(ActonParser.SEMICOLON); }
		public TerminalNode SEMICOLON(int i) {
			return getToken(ActonParser.SEMICOLON, i);
		}
		public MultiConditionContext multiCondition() {
			return getRuleContext(MultiConditionContext.class,0);
		}
		public ForStepContext forStep() {
			return getRuleContext(ForStepContext.class,0);
		}
		public TerminalNode RPAR() { return getToken(ActonParser.RPAR, 0); }
		public MultiStatementContext multiStatement() {
			return getRuleContext(MultiStatementContext.class,0);
		}
		public ForLoopContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_forLoop; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterForLoop(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitForLoop(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitForLoop(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ForLoopContext forLoop() throws RecognitionException {
		ForLoopContext _localctx = new ForLoopContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_forLoop);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(316);
			((ForLoopContext)_localctx).FOR = match(FOR);
			System.out.println("LOOP:" + (((ForLoopContext)_localctx).FOR!=null?((ForLoopContext)_localctx).FOR.getText():null));
			setState(318);
			match(LPAR);
			setState(319);
			forInit();
			setState(320);
			match(SEMICOLON);
			setState(321);
			multiCondition();
			setState(322);
			match(SEMICOLON);
			setState(323);
			forStep();
			setState(324);
			match(RPAR);
			setState(325);
			multiStatement();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ForInitContext extends ParserRuleContext {
		public Token ASSIGN;
		public TerminalNode IDENTIFIER() { return getToken(ActonParser.IDENTIFIER, 0); }
		public TerminalNode ASSIGN() { return getToken(ActonParser.ASSIGN, 0); }
		public TerminalNode NUMBER() { return getToken(ActonParser.NUMBER, 0); }
		public ForInitContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_forInit; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterForInit(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitForInit(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitForInit(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ForInitContext forInit() throws RecognitionException {
		ForInitContext _localctx = new ForInitContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_forInit);
		try {
			setState(332);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case IDENTIFIER:
				enterOuterAlt(_localctx, 1);
				{
				setState(327);
				match(IDENTIFIER);
				setState(328);
				((ForInitContext)_localctx).ASSIGN = match(ASSIGN);
				System.out.println("Operator:" + (((ForInitContext)_localctx).ASSIGN!=null?((ForInitContext)_localctx).ASSIGN.getText():null));
				setState(330);
				match(NUMBER);
				}
				break;
			case SEMICOLON:
				enterOuterAlt(_localctx, 2);
				{
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ForStepContext extends ParserRuleContext {
		public Token ASSIGN;
		public Token INCREMENT;
		public Token DECREMENT;
		public TerminalNode IDENTIFIER() { return getToken(ActonParser.IDENTIFIER, 0); }
		public TerminalNode ASSIGN() { return getToken(ActonParser.ASSIGN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode INCREMENT() { return getToken(ActonParser.INCREMENT, 0); }
		public TerminalNode DECREMENT() { return getToken(ActonParser.DECREMENT, 0); }
		public ForStepContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_forStep; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterForStep(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitForStep(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitForStep(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ForStepContext forStep() throws RecognitionException {
		ForStepContext _localctx = new ForStepContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_forStep);
		try {
			setState(353);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,28,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(334);
				match(IDENTIFIER);
				setState(335);
				((ForStepContext)_localctx).ASSIGN = match(ASSIGN);
				System.out.println("Operator:" + (((ForStepContext)_localctx).ASSIGN!=null?((ForStepContext)_localctx).ASSIGN.getText():null));
				setState(337);
				expression();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(338);
				match(IDENTIFIER);
				setState(343);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case INCREMENT:
					{
					setState(339);
					((ForStepContext)_localctx).INCREMENT = match(INCREMENT);
					System.out.println("Operator:" + (((ForStepContext)_localctx).INCREMENT!=null?((ForStepContext)_localctx).INCREMENT.getText():null));
					}
					break;
				case DECREMENT:
					{
					setState(341);
					((ForStepContext)_localctx).DECREMENT = match(DECREMENT);
					System.out.println("Operator:" + (((ForStepContext)_localctx).DECREMENT!=null?((ForStepContext)_localctx).DECREMENT.getText():null));
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(349);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case INCREMENT:
					{
					setState(345);
					((ForStepContext)_localctx).INCREMENT = match(INCREMENT);
					System.out.println("Operator:" + (((ForStepContext)_localctx).INCREMENT!=null?((ForStepContext)_localctx).INCREMENT.getText():null));
					}
					break;
				case DECREMENT:
					{
					setState(347);
					((ForStepContext)_localctx).DECREMENT = match(DECREMENT);
					System.out.println("Operator:" + (((ForStepContext)_localctx).DECREMENT!=null?((ForStepContext)_localctx).DECREMENT.getText():null));
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(351);
				match(IDENTIFIER);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IfConditionContext extends ParserRuleContext {
		public Token IF;
		public Token ELSE;
		public TerminalNode IF() { return getToken(ActonParser.IF, 0); }
		public TerminalNode LPAR() { return getToken(ActonParser.LPAR, 0); }
		public MultiConditionContext multiCondition() {
			return getRuleContext(MultiConditionContext.class,0);
		}
		public TerminalNode RPAR() { return getToken(ActonParser.RPAR, 0); }
		public List<MultiStatementContext> multiStatement() {
			return getRuleContexts(MultiStatementContext.class);
		}
		public MultiStatementContext multiStatement(int i) {
			return getRuleContext(MultiStatementContext.class,i);
		}
		public TerminalNode ELSE() { return getToken(ActonParser.ELSE, 0); }
		public IfConditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifCondition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterIfCondition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitIfCondition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitIfCondition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IfConditionContext ifCondition() throws RecognitionException {
		IfConditionContext _localctx = new IfConditionContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_ifCondition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(355);
			((IfConditionContext)_localctx).IF = match(IF);
			System.out.println("Conditional:" + (((IfConditionContext)_localctx).IF!=null?((IfConditionContext)_localctx).IF.getText():null));
			setState(357);
			match(LPAR);
			setState(358);
			multiCondition();
			setState(359);
			match(RPAR);
			setState(360);
			multiStatement();
			setState(365);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,29,_ctx) ) {
			case 1:
				{
				setState(361);
				((IfConditionContext)_localctx).ELSE = match(ELSE);
				System.out.println("Conditional:" + (((IfConditionContext)_localctx).ELSE!=null?((IfConditionContext)_localctx).ELSE.getText():null));
				setState(363);
				multiStatement();
				}
				break;
			case 2:
				{
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MultiConditionContext extends ParserRuleContext {
		public Token AND;
		public Token OR;
		public SingleConditionContext singleCondition() {
			return getRuleContext(SingleConditionContext.class,0);
		}
		public TerminalNode AND() { return getToken(ActonParser.AND, 0); }
		public MultiConditionContext multiCondition() {
			return getRuleContext(MultiConditionContext.class,0);
		}
		public TerminalNode OR() { return getToken(ActonParser.OR, 0); }
		public MultiConditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multiCondition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterMultiCondition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitMultiCondition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitMultiCondition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MultiConditionContext multiCondition() throws RecognitionException {
		MultiConditionContext _localctx = new MultiConditionContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_multiCondition);
		try {
			setState(378);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,30,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(367);
				singleCondition();
				setState(368);
				((MultiConditionContext)_localctx).AND = match(AND);
				System.out.println("Operator:" + (((MultiConditionContext)_localctx).AND!=null?((MultiConditionContext)_localctx).AND.getText():null));
				setState(370);
				multiCondition();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(372);
				singleCondition();
				setState(373);
				((MultiConditionContext)_localctx).OR = match(OR);
				System.out.println("Operator:" + (((MultiConditionContext)_localctx).OR!=null?((MultiConditionContext)_localctx).OR.getText():null));
				setState(375);
				multiCondition();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(377);
				singleCondition();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SingleConditionContext extends ParserRuleContext {
		public Token EQUAL;
		public Token NOTEQUAL;
		public Token GREATERTHAN;
		public Token LESSTHAN;
		public Token NOT;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode EQUAL() { return getToken(ActonParser.EQUAL, 0); }
		public TerminalNode NOTEQUAL() { return getToken(ActonParser.NOTEQUAL, 0); }
		public TerminalNode GREATERTHAN() { return getToken(ActonParser.GREATERTHAN, 0); }
		public TerminalNode LESSTHAN() { return getToken(ActonParser.LESSTHAN, 0); }
		public SingleConditionContext singleCondition() {
			return getRuleContext(SingleConditionContext.class,0);
		}
		public TerminalNode BOOLLITERAL() { return getToken(ActonParser.BOOLLITERAL, 0); }
		public TerminalNode IDENTIFIER() { return getToken(ActonParser.IDENTIFIER, 0); }
		public List<TerminalNode> SENDER() { return getTokens(ActonParser.SENDER); }
		public TerminalNode SENDER(int i) {
			return getToken(ActonParser.SENDER, i);
		}
		public List<TerminalNode> STRINGLITERAL() { return getTokens(ActonParser.STRINGLITERAL); }
		public TerminalNode STRINGLITERAL(int i) {
			return getToken(ActonParser.STRINGLITERAL, i);
		}
		public TerminalNode NOT() { return getToken(ActonParser.NOT, 0); }
		public TerminalNode LPAR() { return getToken(ActonParser.LPAR, 0); }
		public TerminalNode RPAR() { return getToken(ActonParser.RPAR, 0); }
		public TerminalNode TRUE() { return getToken(ActonParser.TRUE, 0); }
		public TerminalNode FALSE() { return getToken(ActonParser.FALSE, 0); }
		public SingleConditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_singleCondition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterSingleCondition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitSingleCondition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitSingleCondition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SingleConditionContext singleCondition() throws RecognitionException {
		SingleConditionContext _localctx = new SingleConditionContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_singleCondition);
		int _la;
		try {
			setState(427);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,36,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(380);
				expression();
				setState(389);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case EQUAL:
					{
					setState(381);
					((SingleConditionContext)_localctx).EQUAL = match(EQUAL);
					System.out.println("Operator:" + (((SingleConditionContext)_localctx).EQUAL!=null?((SingleConditionContext)_localctx).EQUAL.getText():null));
					}
					break;
				case NOTEQUAL:
					{
					setState(383);
					((SingleConditionContext)_localctx).NOTEQUAL = match(NOTEQUAL);
					System.out.println("Operator:" + (((SingleConditionContext)_localctx).NOTEQUAL!=null?((SingleConditionContext)_localctx).NOTEQUAL.getText():null));
					}
					break;
				case GREATERTHAN:
					{
					setState(385);
					((SingleConditionContext)_localctx).GREATERTHAN = match(GREATERTHAN);
					System.out.println("Operator:" + (((SingleConditionContext)_localctx).GREATERTHAN!=null?((SingleConditionContext)_localctx).GREATERTHAN.getText():null));
					}
					break;
				case LESSTHAN:
					{
					setState(387);
					((SingleConditionContext)_localctx).LESSTHAN = match(LESSTHAN);
					System.out.println("Operator:" + (((SingleConditionContext)_localctx).LESSTHAN!=null?((SingleConditionContext)_localctx).LESSTHAN.getText():null));
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(391);
				expression();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(395);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,32,_ctx) ) {
				case 1:
					{
					setState(393);
					expression();
					}
					break;
				case 2:
					{
					setState(394);
					match(BOOLLITERAL);
					}
					break;
				}
				setState(401);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case EQUAL:
					{
					setState(397);
					((SingleConditionContext)_localctx).EQUAL = match(EQUAL);
					System.out.println("Operator:" + (((SingleConditionContext)_localctx).EQUAL!=null?((SingleConditionContext)_localctx).EQUAL.getText():null));
					}
					break;
				case NOTEQUAL:
					{
					setState(399);
					((SingleConditionContext)_localctx).NOTEQUAL = match(NOTEQUAL);
					System.out.println("Operator:" + (((SingleConditionContext)_localctx).NOTEQUAL!=null?((SingleConditionContext)_localctx).NOTEQUAL.getText():null));
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(403);
				singleCondition();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(407);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,34,_ctx) ) {
				case 1:
					{
					setState(404);
					expression();
					}
					break;
				case 2:
					{
					setState(405);
					match(SENDER);
					}
					break;
				case 3:
					{
					setState(406);
					match(STRINGLITERAL);
					}
					break;
				}
				setState(413);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case EQUAL:
					{
					setState(409);
					((SingleConditionContext)_localctx).EQUAL = match(EQUAL);
					System.out.println("Operator:" + (((SingleConditionContext)_localctx).EQUAL!=null?((SingleConditionContext)_localctx).EQUAL.getText():null));
					}
					break;
				case NOTEQUAL:
					{
					setState(411);
					((SingleConditionContext)_localctx).NOTEQUAL = match(NOTEQUAL);
					System.out.println("Operator:" + (((SingleConditionContext)_localctx).NOTEQUAL!=null?((SingleConditionContext)_localctx).NOTEQUAL.getText():null));
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(415);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << SENDER) | (1L << STRINGLITERAL) | (1L << IDENTIFIER))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(416);
				((SingleConditionContext)_localctx).NOT = match(NOT);
				System.out.println("Operator:" + (((SingleConditionContext)_localctx).NOT!=null?((SingleConditionContext)_localctx).NOT.getText():null));
				setState(418);
				singleCondition();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(419);
				match(LPAR);
				setState(420);
				singleCondition();
				setState(421);
				match(RPAR);
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(423);
				match(IDENTIFIER);
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(424);
				match(TRUE);
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(425);
				match(FALSE);
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public Token PLUS;
		public Token MINUS;
		public TermContext term() {
			return getRuleContext(TermContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode PLUS() { return getToken(ActonParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(ActonParser.MINUS, 0); }
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		ExpressionContext _localctx = new ExpressionContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_expression);
		try {
			setState(439);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,38,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(429);
				term();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(430);
				term();
				setState(435);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case PLUS:
					{
					setState(431);
					((ExpressionContext)_localctx).PLUS = match(PLUS);
					System.out.println("Operator:" + (((ExpressionContext)_localctx).PLUS!=null?((ExpressionContext)_localctx).PLUS.getText():null));
					}
					break;
				case MINUS:
					{
					setState(433);
					((ExpressionContext)_localctx).MINUS = match(MINUS);
					System.out.println("Operator:" + (((ExpressionContext)_localctx).MINUS!=null?((ExpressionContext)_localctx).MINUS.getText():null));
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(437);
				expression();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TermContext extends ParserRuleContext {
		public Token MULTIPLY;
		public Token DIVIDE;
		public Token MODULO;
		public FactorContext factor() {
			return getRuleContext(FactorContext.class,0);
		}
		public TermContext term() {
			return getRuleContext(TermContext.class,0);
		}
		public TerminalNode MULTIPLY() { return getToken(ActonParser.MULTIPLY, 0); }
		public TerminalNode DIVIDE() { return getToken(ActonParser.DIVIDE, 0); }
		public TerminalNode MODULO() { return getToken(ActonParser.MODULO, 0); }
		public TermContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_term; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterTerm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitTerm(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitTerm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TermContext term() throws RecognitionException {
		TermContext _localctx = new TermContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_term);
		try {
			setState(453);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,40,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(441);
				factor();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(442);
				factor();
				setState(449);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case MULTIPLY:
					{
					setState(443);
					((TermContext)_localctx).MULTIPLY = match(MULTIPLY);
					System.out.println("Operator:" + (((TermContext)_localctx).MULTIPLY!=null?((TermContext)_localctx).MULTIPLY.getText():null));
					}
					break;
				case DIVIDE:
					{
					setState(445);
					((TermContext)_localctx).DIVIDE = match(DIVIDE);
					System.out.println("Operator:" + (((TermContext)_localctx).DIVIDE!=null?((TermContext)_localctx).DIVIDE.getText():null));
					}
					break;
				case MODULO:
					{
					setState(447);
					((TermContext)_localctx).MODULO = match(MODULO);
					System.out.println("Operator:" + (((TermContext)_localctx).MODULO!=null?((TermContext)_localctx).MODULO.getText():null));
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(451);
				term();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FactorContext extends ParserRuleContext {
		public Token MINUS;
		public Token PLUS;
		public PrimaryContext primary() {
			return getRuleContext(PrimaryContext.class,0);
		}
		public FactorContext factor() {
			return getRuleContext(FactorContext.class,0);
		}
		public TerminalNode MINUS() { return getToken(ActonParser.MINUS, 0); }
		public TerminalNode PLUS() { return getToken(ActonParser.PLUS, 0); }
		public FactorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_factor; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterFactor(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitFactor(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitFactor(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FactorContext factor() throws RecognitionException {
		FactorContext _localctx = new FactorContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_factor);
		try {
			setState(463);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case SENDER:
			case SELF:
			case NUMBER:
			case STRINGLITERAL:
			case BOOLLITERAL:
			case LPAR:
			case INCREMENT:
			case DECREMENT:
			case IDENTIFIER:
				enterOuterAlt(_localctx, 1);
				{
				setState(455);
				primary();
				}
				break;
			case MINUS:
			case PLUS:
				enterOuterAlt(_localctx, 2);
				{
				setState(460);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case MINUS:
					{
					setState(456);
					((FactorContext)_localctx).MINUS = match(MINUS);
					System.out.println("Operator:" + (((FactorContext)_localctx).MINUS!=null?((FactorContext)_localctx).MINUS.getText():null));
					}
					break;
				case PLUS:
					{
					setState(458);
					((FactorContext)_localctx).PLUS = match(PLUS);
					System.out.println("Operator:" + (((FactorContext)_localctx).PLUS!=null?((FactorContext)_localctx).PLUS.getText():null));
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(462);
				factor();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrimaryContext extends ParserRuleContext {
		public Token INCREMENT;
		public Token DECREMENT;
		public TerminalNode LPAR() { return getToken(ActonParser.LPAR, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode RPAR() { return getToken(ActonParser.RPAR, 0); }
		public List<TerminalNode> IDENTIFIER() { return getTokens(ActonParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(ActonParser.IDENTIFIER, i);
		}
		public TerminalNode INCREMENT() { return getToken(ActonParser.INCREMENT, 0); }
		public TerminalNode DECREMENT() { return getToken(ActonParser.DECREMENT, 0); }
		public TerminalNode DOT() { return getToken(ActonParser.DOT, 0); }
		public TerminalNode SELF() { return getToken(ActonParser.SELF, 0); }
		public TerminalNode SENDER() { return getToken(ActonParser.SENDER, 0); }
		public TerminalNode NUMBER() { return getToken(ActonParser.NUMBER, 0); }
		public TerminalNode BOOLLITERAL() { return getToken(ActonParser.BOOLLITERAL, 0); }
		public TerminalNode STRINGLITERAL() { return getToken(ActonParser.STRINGLITERAL, 0); }
		public ArrElementContext arrElement() {
			return getRuleContext(ArrElementContext.class,0);
		}
		public PrimaryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_primary; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterPrimary(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitPrimary(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitPrimary(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PrimaryContext primary() throws RecognitionException {
		PrimaryContext _localctx = new PrimaryContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_primary);
		int _la;
		try {
			setState(491);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,45,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(465);
				match(LPAR);
				setState(466);
				expression();
				setState(467);
				match(RPAR);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(469);
				match(IDENTIFIER);
				setState(474);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case INCREMENT:
					{
					setState(470);
					((PrimaryContext)_localctx).INCREMENT = match(INCREMENT);
					System.out.println("Operator:" + (((PrimaryContext)_localctx).INCREMENT!=null?((PrimaryContext)_localctx).INCREMENT.getText():null));
					}
					break;
				case DECREMENT:
					{
					setState(472);
					((PrimaryContext)_localctx).DECREMENT = match(DECREMENT);
					System.out.println("Operator:" + (((PrimaryContext)_localctx).DECREMENT!=null?((PrimaryContext)_localctx).DECREMENT.getText():null));
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(480);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case INCREMENT:
					{
					setState(476);
					((PrimaryContext)_localctx).INCREMENT = match(INCREMENT);
					System.out.println("Operator:" + (((PrimaryContext)_localctx).INCREMENT!=null?((PrimaryContext)_localctx).INCREMENT.getText():null));
					}
					break;
				case DECREMENT:
					{
					setState(478);
					((PrimaryContext)_localctx).DECREMENT = match(DECREMENT);
					System.out.println("Operator:" + (((PrimaryContext)_localctx).DECREMENT!=null?((PrimaryContext)_localctx).DECREMENT.getText():null));
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(482);
				match(IDENTIFIER);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(483);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << SENDER) | (1L << SELF) | (1L << IDENTIFIER))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(484);
				match(DOT);
				setState(485);
				match(IDENTIFIER);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(486);
				match(IDENTIFIER);
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(487);
				match(NUMBER);
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(488);
				match(BOOLLITERAL);
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(489);
				match(STRINGLITERAL);
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(490);
				arrElement();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArrElementContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(ActonParser.IDENTIFIER, 0); }
		public TerminalNode LBRACKET() { return getToken(ActonParser.LBRACKET, 0); }
		public TerminalNode NUMBER() { return getToken(ActonParser.NUMBER, 0); }
		public TerminalNode RBRACKET() { return getToken(ActonParser.RBRACKET, 0); }
		public ArrElementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arrElement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterArrElement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitArrElement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitArrElement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArrElementContext arrElement() throws RecognitionException {
		ArrElementContext _localctx = new ArrElementContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_arrElement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(493);
			match(IDENTIFIER);
			setState(494);
			match(LBRACKET);
			setState(495);
			match(NUMBER);
			setState(496);
			match(RBRACKET);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TertiaryContext extends ParserRuleContext {
		public MultiConditionContext multiCondition() {
			return getRuleContext(MultiConditionContext.class,0);
		}
		public TerminalNode QUESTION() { return getToken(ActonParser.QUESTION, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode COLON() { return getToken(ActonParser.COLON, 0); }
		public TertiaryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tertiary; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterTertiary(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitTertiary(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitTertiary(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TertiaryContext tertiary() throws RecognitionException {
		TertiaryContext _localctx = new TertiaryContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_tertiary);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(498);
			multiCondition();
			setState(499);
			match(QUESTION);
			System.out.println("Operator:?:");
			setState(501);
			expression();
			setState(502);
			match(COLON);
			setState(503);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CallMethodContext extends ParserRuleContext {
		public Token IDENTIFIER;
		public Token SELF;
		public Token SENDER;
		public TerminalNode DOT() { return getToken(ActonParser.DOT, 0); }
		public List<TerminalNode> IDENTIFIER() { return getTokens(ActonParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(ActonParser.IDENTIFIER, i);
		}
		public TerminalNode LPAR() { return getToken(ActonParser.LPAR, 0); }
		public CallArgsContext callArgs() {
			return getRuleContext(CallArgsContext.class,0);
		}
		public TerminalNode RPAR() { return getToken(ActonParser.RPAR, 0); }
		public TerminalNode SEMICOLON() { return getToken(ActonParser.SEMICOLON, 0); }
		public TerminalNode SELF() { return getToken(ActonParser.SELF, 0); }
		public TerminalNode SENDER() { return getToken(ActonParser.SENDER, 0); }
		public CallMethodContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_callMethod; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).enterCallMethod(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ActonListener ) ((ActonListener)listener).exitCallMethod(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ActonVisitor ) return ((ActonVisitor<? extends T>)visitor).visitCallMethod(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CallMethodContext callMethod() throws RecognitionException {
		CallMethodContext _localctx = new CallMethodContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_callMethod);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(511);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case IDENTIFIER:
				{
				setState(505);
				((CallMethodContext)_localctx).IDENTIFIER = match(IDENTIFIER);
				System.out.print((((CallMethodContext)_localctx).IDENTIFIER!=null?((CallMethodContext)_localctx).IDENTIFIER.getText():null));
				}
				break;
			case SELF:
				{
				setState(507);
				((CallMethodContext)_localctx).SELF = match(SELF);
				System.out.print((((CallMethodContext)_localctx).SELF!=null?((CallMethodContext)_localctx).SELF.getText():null));
				}
				break;
			case SENDER:
				{
				setState(509);
				((CallMethodContext)_localctx).SENDER = match(SENDER);
				System.out.print((((CallMethodContext)_localctx).SENDER!=null?((CallMethodContext)_localctx).SENDER.getText():null));
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(513);
			match(DOT);
			setState(514);
			((CallMethodContext)_localctx).IDENTIFIER = match(IDENTIFIER);
			System.out.println("," + (((CallMethodContext)_localctx).IDENTIFIER!=null?((CallMethodContext)_localctx).IDENTIFIER.getText():null));
			setState(516);
			match(LPAR);
			setState(517);
			callArgs();
			setState(518);
			match(RPAR);
			setState(519);
			match(SEMICOLON);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\67\u020c\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\3\2\3\2\3\2\3\3\3\3\3\3\3"+
		"\3\5\3V\n\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4_\n\4\3\4\3\4\3\4\3\4\3\4\3"+
		"\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\5\5n\n\5\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3"+
		"\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\5\b\u0080\n\b\3\t\3\t\3\t\3\t\3\t\3"+
		"\n\3\n\3\n\3\n\5\n\u008b\n\n\3\13\3\13\3\13\3\13\5\13\u0091\n\13\3\f\3"+
		"\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\5\r\u00a1\n\r\3\16"+
		"\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\17\3\17\3\20\3\20\3\20"+
		"\3\20\5\20\u00b3\n\20\3\20\3\20\5\20\u00b7\n\20\3\21\3\21\3\21\3\21\3"+
		"\21\5\21\u00be\n\21\3\21\3\21\5\21\u00c2\n\21\3\22\3\22\3\22\3\22\3\22"+
		"\3\22\3\22\5\22\u00cb\n\22\3\22\3\22\3\22\3\22\3\22\5\22\u00d2\n\22\3"+
		"\22\3\22\3\22\3\22\5\22\u00d8\n\22\3\23\3\23\3\23\3\23\3\23\3\23\3\23"+
		"\5\23\u00e1\n\23\3\24\3\24\3\24\3\24\5\24\u00e7\n\24\3\24\3\24\3\24\3"+
		"\24\3\24\3\24\5\24\u00ef\n\24\3\24\5\24\u00f2\n\24\3\25\3\25\3\25\3\25"+
		"\3\25\5\25\u00f9\n\25\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\5\26\u0103"+
		"\n\26\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\5\27"+
		"\u0111\n\27\3\30\3\30\3\30\3\30\3\30\3\30\3\30\5\30\u011a\n\30\3\30\3"+
		"\30\3\30\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\5\31\u012a"+
		"\n\31\3\32\3\32\3\32\3\32\3\32\5\32\u0131\n\32\3\32\3\32\3\32\3\32\3\32"+
		"\3\32\3\32\3\32\5\32\u013b\n\32\3\32\3\32\3\33\3\33\3\33\3\33\3\33\3\33"+
		"\3\33\3\33\3\33\3\33\3\33\3\34\3\34\3\34\3\34\3\34\5\34\u014f\n\34\3\35"+
		"\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\5\35\u015a\n\35\3\35\3\35\3\35"+
		"\3\35\5\35\u0160\n\35\3\35\3\35\5\35\u0164\n\35\3\36\3\36\3\36\3\36\3"+
		"\36\3\36\3\36\3\36\3\36\3\36\5\36\u0170\n\36\3\37\3\37\3\37\3\37\3\37"+
		"\3\37\3\37\3\37\3\37\3\37\3\37\5\37\u017d\n\37\3 \3 \3 \3 \3 \3 \3 \3"+
		" \3 \5 \u0188\n \3 \3 \3 \3 \5 \u018e\n \3 \3 \3 \3 \5 \u0194\n \3 \3"+
		" \3 \3 \5 \u019a\n \3 \3 \3 \3 \5 \u01a0\n \3 \3 \3 \3 \3 \3 \3 \3 \3"+
		" \3 \3 \3 \5 \u01ae\n \3!\3!\3!\3!\3!\3!\5!\u01b6\n!\3!\3!\5!\u01ba\n"+
		"!\3\"\3\"\3\"\3\"\3\"\3\"\3\"\3\"\5\"\u01c4\n\"\3\"\3\"\5\"\u01c8\n\""+
		"\3#\3#\3#\3#\3#\5#\u01cf\n#\3#\5#\u01d2\n#\3$\3$\3$\3$\3$\3$\3$\3$\3$"+
		"\5$\u01dd\n$\3$\3$\3$\3$\5$\u01e3\n$\3$\3$\3$\3$\3$\3$\3$\3$\3$\5$\u01ee"+
		"\n$\3%\3%\3%\3%\3%\3&\3&\3&\3&\3&\3&\3&\3\'\3\'\3\'\3\'\3\'\3\'\5\'\u0202"+
		"\n\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\2\2(\2\4\6\b\n\f\16\20\22\24"+
		"\26\30\32\34\36 \"$&(*,.\60\62\64\668:<>@BDFHJL\2\4\5\2\r\r\21\21\65\65"+
		"\4\2\r\16\65\65\2\u023e\2N\3\2\2\2\4U\3\2\2\2\6W\3\2\2\2\bm\3\2\2\2\n"+
		"o\3\2\2\2\fs\3\2\2\2\16\177\3\2\2\2\20\u0081\3\2\2\2\22\u008a\3\2\2\2"+
		"\24\u0090\3\2\2\2\26\u0092\3\2\2\2\30\u00a0\3\2\2\2\32\u00a2\3\2\2\2\34"+
		"\u00ac\3\2\2\2\36\u00b6\3\2\2\2 \u00c1\3\2\2\2\"\u00d7\3\2\2\2$\u00e0"+
		"\3\2\2\2&\u00f1\3\2\2\2(\u00f8\3\2\2\2*\u0102\3\2\2\2,\u0110\3\2\2\2."+
		"\u0112\3\2\2\2\60\u0129\3\2\2\2\62\u012b\3\2\2\2\64\u013e\3\2\2\2\66\u014e"+
		"\3\2\2\28\u0163\3\2\2\2:\u0165\3\2\2\2<\u017c\3\2\2\2>\u01ad\3\2\2\2@"+
		"\u01b9\3\2\2\2B\u01c7\3\2\2\2D\u01d1\3\2\2\2F\u01ed\3\2\2\2H\u01ef\3\2"+
		"\2\2J\u01f4\3\2\2\2L\u0201\3\2\2\2NO\5\4\3\2OP\7\2\2\3P\3\3\2\2\2QR\5"+
		"\6\4\2RS\5\4\3\2SV\3\2\2\2TV\5\b\5\2UQ\3\2\2\2UT\3\2\2\2V\5\3\2\2\2WX"+
		"\7\b\2\2XY\7\65\2\2Y^\b\4\1\2Z[\7\5\2\2[\\\7\65\2\2\\_\b\4\1\2]_\b\4\1"+
		"\2^Z\3\2\2\2^]\3\2\2\2_`\3\2\2\2`a\7\"\2\2ab\7\20\2\2bc\7#\2\2cd\7 \2"+
		"\2de\5\n\6\2ef\7!\2\2f\7\3\2\2\2gh\7\17\2\2hi\7 \2\2ij\5\"\22\2jk\7!\2"+
		"\2kn\3\2\2\2ln\3\2\2\2mg\3\2\2\2ml\3\2\2\2n\t\3\2\2\2op\5\f\7\2pq\5\20"+
		"\t\2qr\5\24\13\2r\13\3\2\2\2st\7\7\2\2tu\7 \2\2uv\5\16\b\2vw\7!\2\2w\r"+
		"\3\2\2\2xy\7\65\2\2yz\b\b\1\2z{\7\65\2\2{|\b\b\1\2|}\7\34\2\2}\u0080\5"+
		"\16\b\2~\u0080\3\2\2\2\177x\3\2\2\2\177~\3\2\2\2\u0080\17\3\2\2\2\u0081"+
		"\u0082\7\6\2\2\u0082\u0083\7 \2\2\u0083\u0084\5\22\n\2\u0084\u0085\7!"+
		"\2\2\u0085\21\3\2\2\2\u0086\u0087\5\60\31\2\u0087\u0088\5\22\n\2\u0088"+
		"\u008b\3\2\2\2\u0089\u008b\3\2\2\2\u008a\u0086\3\2\2\2\u008a\u0089\3\2"+
		"\2\2\u008b\23\3\2\2\2\u008c\u008d\5\26\f\2\u008d\u008e\5\30\r\2\u008e"+
		"\u0091\3\2\2\2\u008f\u0091\5\30\r\2\u0090\u008c\3\2\2\2\u0090\u008f\3"+
		"\2\2\2\u0091\25\3\2\2\2\u0092\u0093\7\3\2\2\u0093\u0094\7\4\2\2\u0094"+
		"\u0095\b\f\1\2\u0095\u0096\7\"\2\2\u0096\u0097\5\36\20\2\u0097\u0098\7"+
		"#\2\2\u0098\u0099\7 \2\2\u0099\u009a\5\34\17\2\u009a\u009b\7!\2\2\u009b"+
		"\27\3\2\2\2\u009c\u009d\5\32\16\2\u009d\u009e\5\30\r\2\u009e\u00a1\3\2"+
		"\2\2\u009f\u00a1\3\2\2\2\u00a0\u009c\3\2\2\2\u00a0\u009f\3\2\2\2\u00a1"+
		"\31\3\2\2\2\u00a2\u00a3\7\3\2\2\u00a3\u00a4\7\65\2\2\u00a4\u00a5\b\16"+
		"\1\2\u00a5\u00a6\7\"\2\2\u00a6\u00a7\5\36\20\2\u00a7\u00a8\7#\2\2\u00a8"+
		"\u00a9\7 \2\2\u00a9\u00aa\5\34\17\2\u00aa\u00ab\7!\2\2\u00ab\33\3\2\2"+
		"\2\u00ac\u00ad\5*\26\2\u00ad\35\3\2\2\2\u00ae\u00b2\7\23\2\2\u00af\u00b0"+
		"\7\65\2\2\u00b0\u00b3\b\20\1\2\u00b1\u00b3\5H%\2\u00b2\u00af\3\2\2\2\u00b2"+
		"\u00b1\3\2\2\2\u00b3\u00b4\3\2\2\2\u00b4\u00b7\5 \21\2\u00b5\u00b7\b\20"+
		"\1\2\u00b6\u00ae\3\2\2\2\u00b6\u00b5\3\2\2\2\u00b7\37\3\2\2\2\u00b8\u00b9"+
		"\7\35\2\2\u00b9\u00bd\7\23\2\2\u00ba\u00bb\7\65\2\2\u00bb\u00be\b\21\1"+
		"\2\u00bc\u00be\5H%\2\u00bd\u00ba\3\2\2\2\u00bd\u00bc\3\2\2\2\u00be\u00bf"+
		"\3\2\2\2\u00bf\u00c2\5 \21\2\u00c0\u00c2\b\21\1\2\u00c1\u00b8\3\2\2\2"+
		"\u00c1\u00c0\3\2\2\2\u00c2!\3\2\2\2\u00c3\u00c4\7\65\2\2\u00c4\u00c5\b"+
		"\22\1\2\u00c5\u00c6\7\65\2\2\u00c6\u00c7\b\22\1\2\u00c7\u00ca\7\"\2\2"+
		"\u00c8\u00cb\5$\23\2\u00c9\u00cb\b\22\1\2\u00ca\u00c8\3\2\2\2\u00ca\u00c9"+
		"\3\2\2\2\u00cb\u00cc\3\2\2\2\u00cc\u00cd\7#\2\2\u00cd\u00ce\7\33\2\2\u00ce"+
		"\u00d1\7\"\2\2\u00cf\u00d2\5&\24\2\u00d0\u00d2\3\2\2\2\u00d1\u00cf\3\2"+
		"\2\2\u00d1\u00d0\3\2\2\2\u00d2\u00d3\3\2\2\2\u00d3\u00d4\7#\2\2\u00d4"+
		"\u00d5\7\34\2\2\u00d5\u00d8\5\"\22\2\u00d6\u00d8\3\2\2\2\u00d7\u00c3\3"+
		"\2\2\2\u00d7\u00d6\3\2\2\2\u00d8#\3\2\2\2\u00d9\u00da\7\65\2\2\u00da\u00db"+
		"\b\23\1\2\u00db\u00dc\7\35\2\2\u00dc\u00e1\5$\23\2\u00dd\u00de\7\65\2"+
		"\2\u00de\u00e1\b\23\1\2\u00df\u00e1\b\23\1\2\u00e0\u00d9\3\2\2\2\u00e0"+
		"\u00dd\3\2\2\2\u00e0\u00df\3\2\2\2\u00e1%\3\2\2\2\u00e2\u00e7\5@!\2\u00e3"+
		"\u00e7\7\20\2\2\u00e4\u00e7\7\21\2\2\u00e5\u00e7\7\22\2\2\u00e6\u00e2"+
		"\3\2\2\2\u00e6\u00e3\3\2\2\2\u00e6\u00e4\3\2\2\2\u00e6\u00e5\3\2\2\2\u00e7"+
		"\u00e8\3\2\2\2\u00e8\u00e9\7\35\2\2\u00e9\u00f2\5&\24\2\u00ea\u00ef\5"+
		"@!\2\u00eb\u00ef\7\20\2\2\u00ec\u00ef\7\21\2\2\u00ed\u00ef\7\22\2\2\u00ee"+
		"\u00ea\3\2\2\2\u00ee\u00eb\3\2\2\2\u00ee\u00ec\3\2\2\2\u00ee\u00ed\3\2"+
		"\2\2\u00ef\u00f2\3\2\2\2\u00f0\u00f2\3\2\2\2\u00f1\u00e6\3\2\2\2\u00f1"+
		"\u00ee\3\2\2\2\u00f1\u00f0\3\2\2\2\u00f2\'\3\2\2\2\u00f3\u00f4\7 \2\2"+
		"\u00f4\u00f5\5*\26\2\u00f5\u00f6\7!\2\2\u00f6\u00f9\3\2\2\2\u00f7\u00f9"+
		"\5,\27\2\u00f8\u00f3\3\2\2\2\u00f8\u00f7\3\2\2\2\u00f9)\3\2\2\2\u00fa"+
		"\u00fb\5,\27\2\u00fb\u00fc\5*\26\2\u00fc\u0103\3\2\2\2\u00fd\u00fe\7 "+
		"\2\2\u00fe\u00ff\5*\26\2\u00ff\u0100\7!\2\2\u0100\u0103\3\2\2\2\u0101"+
		"\u0103\3\2\2\2\u0102\u00fa\3\2\2\2\u0102\u00fd\3\2\2\2\u0102\u0101\3\2"+
		"\2\2\u0103+\3\2\2\2\u0104\u0111\5\60\31\2\u0105\u0111\5\62\32\2\u0106"+
		"\u0111\5\64\33\2\u0107\u0111\5:\36\2\u0108\u0109\b\27\1\2\u0109\u0111"+
		"\5L\'\2\u010a\u010b\7\32\2\2\u010b\u0111\7\34\2\2\u010c\u010d\7\31\2\2"+
		"\u010d\u0111\7\34\2\2\u010e\u0111\5.\30\2\u010f\u0111\7\34\2\2\u0110\u0104"+
		"\3\2\2\2\u0110\u0105\3\2\2\2\u0110\u0106\3\2\2\2\u0110\u0107\3\2\2\2\u0110"+
		"\u0108\3\2\2\2\u0110\u010a\3\2\2\2\u0110\u010c\3\2\2\2\u0110\u010e\3\2"+
		"\2\2\u0110\u010f\3\2\2\2\u0111-\3\2\2\2\u0112\u0113\7\t\2\2\u0113\u0114"+
		"\b\30\1\2\u0114\u0119\7\"\2\2\u0115\u011a\5@!\2\u0116\u011a\7\22\2\2\u0117"+
		"\u011a\7\21\2\2\u0118\u011a\7\20\2\2\u0119\u0115\3\2\2\2\u0119\u0116\3"+
		"\2\2\2\u0119\u0117\3\2\2\2\u0119\u0118\3\2\2\2\u011a\u011b\3\2\2\2\u011b"+
		"\u011c\7#\2\2\u011c\u011d\7\34\2\2\u011d/\3\2\2\2\u011e\u011f\7\23\2\2"+
		"\u011f\u0120\7\65\2\2\u0120\u0121\b\31\1\2\u0121\u012a\7\34\2\2\u0122"+
		"\u0123\7\26\2\2\u0123\u0124\7\65\2\2\u0124\u0125\7$\2\2\u0125\u0126\7"+
		"\20\2\2\u0126\u0127\7%\2\2\u0127\u0128\b\31\1\2\u0128\u012a\7\34\2\2\u0129"+
		"\u011e\3\2\2\2\u0129\u0122\3\2\2\2\u012a\61\3\2\2\2\u012b\u0130\7\65\2"+
		"\2\u012c\u012d\7$\2\2\u012d\u012e\7\20\2\2\u012e\u0131\7%\2\2\u012f\u0131"+
		"\3\2\2\2\u0130\u012c\3\2\2\2\u0130\u012f\3\2\2\2\u0131\u0132\3\2\2\2\u0132"+
		"\u0133\7\64\2\2\u0133\u013a\b\32\1\2\u0134\u013b\5@!\2\u0135\u013b\5<"+
		"\37\2\u0136\u013b\7\20\2\2\u0137\u013b\7\21\2\2\u0138\u013b\7\22\2\2\u0139"+
		"\u013b\5J&\2\u013a\u0134\3\2\2\2\u013a\u0135\3\2\2\2\u013a\u0136\3\2\2"+
		"\2\u013a\u0137\3\2\2\2\u013a\u0138\3\2\2\2\u013a\u0139\3\2\2\2\u013b\u013c"+
		"\3\2\2\2\u013c\u013d\7\34\2\2\u013d\63\3\2\2\2\u013e\u013f\7\n\2\2\u013f"+
		"\u0140\b\33\1\2\u0140\u0141\7\"\2\2\u0141\u0142\5\66\34\2\u0142\u0143"+
		"\7\34\2\2\u0143\u0144\5<\37\2\u0144\u0145\7\34\2\2\u0145\u0146\58\35\2"+
		"\u0146\u0147\7#\2\2\u0147\u0148\5(\25\2\u0148\65\3\2\2\2\u0149\u014a\7"+
		"\65\2\2\u014a\u014b\7\64\2\2\u014b\u014c\b\34\1\2\u014c\u014f\7\20\2\2"+
		"\u014d\u014f\3\2\2\2\u014e\u0149\3\2\2\2\u014e\u014d\3\2\2\2\u014f\67"+
		"\3\2\2\2\u0150\u0151\7\65\2\2\u0151\u0152\7\64\2\2\u0152\u0153\b\35\1"+
		"\2\u0153\u0164\5@!\2\u0154\u0159\7\65\2\2\u0155\u0156\7&\2\2\u0156\u015a"+
		"\b\35\1\2\u0157\u0158\7\'\2\2\u0158\u015a\b\35\1\2\u0159\u0155\3\2\2\2"+
		"\u0159\u0157\3\2\2\2\u015a\u0164\3\2\2\2\u015b\u015c\7&\2\2\u015c\u0160"+
		"\b\35\1\2\u015d\u015e\7\'\2\2\u015e\u0160\b\35\1\2\u015f\u015b\3\2\2\2"+
		"\u015f\u015d\3\2\2\2\u0160\u0161\3\2\2\2\u0161\u0164\7\65\2\2\u0162\u0164"+
		"\3\2\2\2\u0163\u0150\3\2\2\2\u0163\u0154\3\2\2\2\u0163\u015f\3\2\2\2\u0163"+
		"\u0162\3\2\2\2\u01649\3\2\2\2\u0165\u0166\7\f\2\2\u0166\u0167\b\36\1\2"+
		"\u0167\u0168\7\"\2\2\u0168\u0169\5<\37\2\u0169\u016a\7#\2\2\u016a\u016f"+
		"\5(\25\2\u016b\u016c\7\13\2\2\u016c\u016d\b\36\1\2\u016d\u0170\5(\25\2"+
		"\u016e\u0170\3\2\2\2\u016f\u016b\3\2\2\2\u016f\u016e\3\2\2\2\u0170;\3"+
		"\2\2\2\u0171\u0172\5> \2\u0172\u0173\7\61\2\2\u0173\u0174\b\37\1\2\u0174"+
		"\u0175\5<\37\2\u0175\u017d\3\2\2\2\u0176\u0177\5> \2\u0177\u0178\7\62"+
		"\2\2\u0178\u0179\b\37\1\2\u0179\u017a\5<\37\2\u017a\u017d\3\2\2\2\u017b"+
		"\u017d\5> \2\u017c\u0171\3\2\2\2\u017c\u0176\3\2\2\2\u017c\u017b\3\2\2"+
		"\2\u017d=\3\2\2\2\u017e\u0187\5@!\2\u017f\u0180\7-\2\2\u0180\u0188\b "+
		"\1\2\u0181\u0182\7.\2\2\u0182\u0188\b \1\2\u0183\u0184\7/\2\2\u0184\u0188"+
		"\b \1\2\u0185\u0186\7\60\2\2\u0186\u0188\b \1\2\u0187\u017f\3\2\2\2\u0187"+
		"\u0181\3\2\2\2\u0187\u0183\3\2\2\2\u0187\u0185\3\2\2\2\u0188\u0189\3\2"+
		"\2\2\u0189\u018a\5@!\2\u018a\u01ae\3\2\2\2\u018b\u018e\5@!\2\u018c\u018e"+
		"\7\22\2\2\u018d\u018b\3\2\2\2\u018d\u018c\3\2\2\2\u018e\u0193\3\2\2\2"+
		"\u018f\u0190\7-\2\2\u0190\u0194\b \1\2\u0191\u0192\7.\2\2\u0192\u0194"+
		"\b \1\2\u0193\u018f\3\2\2\2\u0193\u0191\3\2\2\2\u0194\u0195\3\2\2\2\u0195"+
		"\u01ae\5> \2\u0196\u019a\5@!\2\u0197\u019a\7\r\2\2\u0198\u019a\7\21\2"+
		"\2\u0199\u0196\3\2\2\2\u0199\u0197\3\2\2\2\u0199\u0198\3\2\2\2\u019a\u019f"+
		"\3\2\2\2\u019b\u019c\7-\2\2\u019c\u01a0\b \1\2\u019d\u019e\7.\2\2\u019e"+
		"\u01a0\b \1\2\u019f\u019b\3\2\2\2\u019f\u019d\3\2\2\2\u01a0\u01a1\3\2"+
		"\2\2\u01a1\u01ae\t\2\2\2\u01a2\u01a3\7\63\2\2\u01a3\u01a4\b \1\2\u01a4"+
		"\u01ae\5> \2\u01a5\u01a6\7\"\2\2\u01a6\u01a7\5> \2\u01a7\u01a8\7#\2\2"+
		"\u01a8\u01ae\3\2\2\2\u01a9\u01ae\7\65\2\2\u01aa\u01ae\7\27\2\2\u01ab\u01ae"+
		"\7\30\2\2\u01ac\u01ae\3\2\2\2\u01ad\u017e\3\2\2\2\u01ad\u018d\3\2\2\2"+
		"\u01ad\u0199\3\2\2\2\u01ad\u01a2\3\2\2\2\u01ad\u01a5\3\2\2\2\u01ad\u01a9"+
		"\3\2\2\2\u01ad\u01aa\3\2\2\2\u01ad\u01ab\3\2\2\2\u01ad\u01ac\3\2\2\2\u01ae"+
		"?\3\2\2\2\u01af\u01ba\5B\"\2\u01b0\u01b5\5B\"\2\u01b1\u01b2\7)\2\2\u01b2"+
		"\u01b6\b!\1\2\u01b3\u01b4\7(\2\2\u01b4\u01b6\b!\1\2\u01b5\u01b1\3\2\2"+
		"\2\u01b5\u01b3\3\2\2\2\u01b6\u01b7\3\2\2\2\u01b7\u01b8\5@!\2\u01b8\u01ba"+
		"\3\2\2\2\u01b9\u01af\3\2\2\2\u01b9\u01b0\3\2\2\2\u01baA\3\2\2\2\u01bb"+
		"\u01c8\5D#\2\u01bc\u01c3\5D#\2\u01bd\u01be\7*\2\2\u01be\u01c4\b\"\1\2"+
		"\u01bf\u01c0\7+\2\2\u01c0\u01c4\b\"\1\2\u01c1\u01c2\7,\2\2\u01c2\u01c4"+
		"\b\"\1\2\u01c3\u01bd\3\2\2\2\u01c3\u01bf\3\2\2\2\u01c3\u01c1\3\2\2\2\u01c4"+
		"\u01c5\3\2\2\2\u01c5\u01c6\5B\"\2\u01c6\u01c8\3\2\2\2\u01c7\u01bb\3\2"+
		"\2\2\u01c7\u01bc\3\2\2\2\u01c8C\3\2\2\2\u01c9\u01d2\5F$\2\u01ca\u01cb"+
		"\7(\2\2\u01cb\u01cf\b#\1\2\u01cc\u01cd\7)\2\2\u01cd\u01cf\b#\1\2\u01ce"+
		"\u01ca\3\2\2\2\u01ce\u01cc\3\2\2\2\u01cf\u01d0\3\2\2\2\u01d0\u01d2\5D"+
		"#\2\u01d1\u01c9\3\2\2\2\u01d1\u01ce\3\2\2\2\u01d2E\3\2\2\2\u01d3\u01d4"+
		"\7\"\2\2\u01d4\u01d5\5@!\2\u01d5\u01d6\7#\2\2\u01d6\u01ee\3\2\2\2\u01d7"+
		"\u01dc\7\65\2\2\u01d8\u01d9\7&\2\2\u01d9\u01dd\b$\1\2\u01da\u01db\7\'"+
		"\2\2\u01db\u01dd\b$\1\2\u01dc\u01d8\3\2\2\2\u01dc\u01da\3\2\2\2\u01dd"+
		"\u01ee\3\2\2\2\u01de\u01df\7&\2\2\u01df\u01e3\b$\1\2\u01e0\u01e1\7\'\2"+
		"\2\u01e1\u01e3\b$\1\2\u01e2\u01de\3\2\2\2\u01e2\u01e0\3\2\2\2\u01e3\u01e4"+
		"\3\2\2\2\u01e4\u01ee\7\65\2\2\u01e5\u01e6\t\3\2\2\u01e6\u01e7\7\36\2\2"+
		"\u01e7\u01ee\7\65\2\2\u01e8\u01ee\7\65\2\2\u01e9\u01ee\7\20\2\2\u01ea"+
		"\u01ee\7\22\2\2\u01eb\u01ee\7\21\2\2\u01ec\u01ee\5H%\2\u01ed\u01d3\3\2"+
		"\2\2\u01ed\u01d7\3\2\2\2\u01ed\u01e2\3\2\2\2\u01ed\u01e5\3\2\2\2\u01ed"+
		"\u01e8\3\2\2\2\u01ed\u01e9\3\2\2\2\u01ed\u01ea\3\2\2\2\u01ed\u01eb\3\2"+
		"\2\2\u01ed\u01ec\3\2\2\2\u01eeG\3\2\2\2\u01ef\u01f0\7\65\2\2\u01f0\u01f1"+
		"\7$\2\2\u01f1\u01f2\7\20\2\2\u01f2\u01f3\7%\2\2\u01f3I\3\2\2\2\u01f4\u01f5"+
		"\5<\37\2\u01f5\u01f6\7\37\2\2\u01f6\u01f7\b&\1\2\u01f7\u01f8\5@!\2\u01f8"+
		"\u01f9\7\33\2\2\u01f9\u01fa\5@!\2\u01faK\3\2\2\2\u01fb\u01fc\7\65\2\2"+
		"\u01fc\u0202\b\'\1\2\u01fd\u01fe\7\16\2\2\u01fe\u0202\b\'\1\2\u01ff\u0200"+
		"\7\r\2\2\u0200\u0202\b\'\1\2\u0201\u01fb\3\2\2\2\u0201\u01fd\3\2\2\2\u0201"+
		"\u01ff\3\2\2\2\u0202\u0203\3\2\2\2\u0203\u0204\7\36\2\2\u0204\u0205\7"+
		"\65\2\2\u0205\u0206\b\'\1\2\u0206\u0207\7\"\2\2\u0207\u0208\5&\24\2\u0208"+
		"\u0209\7#\2\2\u0209\u020a\7\34\2\2\u020aM\3\2\2\2\61U^m\177\u008a\u0090"+
		"\u00a0\u00b2\u00b6\u00bd\u00c1\u00ca\u00d1\u00d7\u00e0\u00e6\u00ee\u00f1"+
		"\u00f8\u0102\u0110\u0119\u0129\u0130\u013a\u014e\u0159\u015f\u0163\u016f"+
		"\u017c\u0187\u018d\u0193\u0199\u019f\u01ad\u01b5\u01b9\u01c3\u01c7\u01ce"+
		"\u01d1\u01dc\u01e2\u01ed\u0201";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}