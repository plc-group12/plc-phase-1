// Generated from D:/PLC/Acton/src\Acton.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ActonParser}.
 */
public interface ActonListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link ActonParser#acton}.
	 * @param ctx the parse tree
	 */
	void enterActon(ActonParser.ActonContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#acton}.
	 * @param ctx the parse tree
	 */
	void exitActon(ActonParser.ActonContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(ActonParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(ActonParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#actor}.
	 * @param ctx the parse tree
	 */
	void enterActor(ActonParser.ActorContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#actor}.
	 * @param ctx the parse tree
	 */
	void exitActor(ActonParser.ActorContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#main}.
	 * @param ctx the parse tree
	 */
	void enterMain(ActonParser.MainContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#main}.
	 * @param ctx the parse tree
	 */
	void exitMain(ActonParser.MainContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#actorBody}.
	 * @param ctx the parse tree
	 */
	void enterActorBody(ActonParser.ActorBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#actorBody}.
	 * @param ctx the parse tree
	 */
	void exitActorBody(ActonParser.ActorBodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#knownActors}.
	 * @param ctx the parse tree
	 */
	void enterKnownActors(ActonParser.KnownActorsContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#knownActors}.
	 * @param ctx the parse tree
	 */
	void exitKnownActors(ActonParser.KnownActorsContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#knownActorsBody}.
	 * @param ctx the parse tree
	 */
	void enterKnownActorsBody(ActonParser.KnownActorsBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#knownActorsBody}.
	 * @param ctx the parse tree
	 */
	void exitKnownActorsBody(ActonParser.KnownActorsBodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#actorVars}.
	 * @param ctx the parse tree
	 */
	void enterActorVars(ActonParser.ActorVarsContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#actorVars}.
	 * @param ctx the parse tree
	 */
	void exitActorVars(ActonParser.ActorVarsContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#actorVarsBody}.
	 * @param ctx the parse tree
	 */
	void enterActorVarsBody(ActonParser.ActorVarsBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#actorVarsBody}.
	 * @param ctx the parse tree
	 */
	void exitActorVarsBody(ActonParser.ActorVarsBodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#msgHandlers}.
	 * @param ctx the parse tree
	 */
	void enterMsgHandlers(ActonParser.MsgHandlersContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#msgHandlers}.
	 * @param ctx the parse tree
	 */
	void exitMsgHandlers(ActonParser.MsgHandlersContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#initial}.
	 * @param ctx the parse tree
	 */
	void enterInitial(ActonParser.InitialContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#initial}.
	 * @param ctx the parse tree
	 */
	void exitInitial(ActonParser.InitialContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#initLess}.
	 * @param ctx the parse tree
	 */
	void enterInitLess(ActonParser.InitLessContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#initLess}.
	 * @param ctx the parse tree
	 */
	void exitInitLess(ActonParser.InitLessContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#msgHandler}.
	 * @param ctx the parse tree
	 */
	void enterMsgHandler(ActonParser.MsgHandlerContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#msgHandler}.
	 * @param ctx the parse tree
	 */
	void exitMsgHandler(ActonParser.MsgHandlerContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#msgHandlerBody}.
	 * @param ctx the parse tree
	 */
	void enterMsgHandlerBody(ActonParser.MsgHandlerBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#msgHandlerBody}.
	 * @param ctx the parse tree
	 */
	void exitMsgHandlerBody(ActonParser.MsgHandlerBodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#declareArgs}.
	 * @param ctx the parse tree
	 */
	void enterDeclareArgs(ActonParser.DeclareArgsContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#declareArgs}.
	 * @param ctx the parse tree
	 */
	void exitDeclareArgs(ActonParser.DeclareArgsContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#declareArgsFollowing}.
	 * @param ctx the parse tree
	 */
	void enterDeclareArgsFollowing(ActonParser.DeclareArgsFollowingContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#declareArgsFollowing}.
	 * @param ctx the parse tree
	 */
	void exitDeclareArgsFollowing(ActonParser.DeclareArgsFollowingContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#mainBody}.
	 * @param ctx the parse tree
	 */
	void enterMainBody(ActonParser.MainBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#mainBody}.
	 * @param ctx the parse tree
	 */
	void exitMainBody(ActonParser.MainBodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#callKnownActorsArgs}.
	 * @param ctx the parse tree
	 */
	void enterCallKnownActorsArgs(ActonParser.CallKnownActorsArgsContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#callKnownActorsArgs}.
	 * @param ctx the parse tree
	 */
	void exitCallKnownActorsArgs(ActonParser.CallKnownActorsArgsContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#callArgs}.
	 * @param ctx the parse tree
	 */
	void enterCallArgs(ActonParser.CallArgsContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#callArgs}.
	 * @param ctx the parse tree
	 */
	void exitCallArgs(ActonParser.CallArgsContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#multiStatement}.
	 * @param ctx the parse tree
	 */
	void enterMultiStatement(ActonParser.MultiStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#multiStatement}.
	 * @param ctx the parse tree
	 */
	void exitMultiStatement(ActonParser.MultiStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#multiStatementInBraces}.
	 * @param ctx the parse tree
	 */
	void enterMultiStatementInBraces(ActonParser.MultiStatementInBracesContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#multiStatementInBraces}.
	 * @param ctx the parse tree
	 */
	void exitMultiStatementInBraces(ActonParser.MultiStatementInBracesContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#singleStatement}.
	 * @param ctx the parse tree
	 */
	void enterSingleStatement(ActonParser.SingleStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#singleStatement}.
	 * @param ctx the parse tree
	 */
	void exitSingleStatement(ActonParser.SingleStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#print}.
	 * @param ctx the parse tree
	 */
	void enterPrint(ActonParser.PrintContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#print}.
	 * @param ctx the parse tree
	 */
	void exitPrint(ActonParser.PrintContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#declareVar}.
	 * @param ctx the parse tree
	 */
	void enterDeclareVar(ActonParser.DeclareVarContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#declareVar}.
	 * @param ctx the parse tree
	 */
	void exitDeclareVar(ActonParser.DeclareVarContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#assignment}.
	 * @param ctx the parse tree
	 */
	void enterAssignment(ActonParser.AssignmentContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#assignment}.
	 * @param ctx the parse tree
	 */
	void exitAssignment(ActonParser.AssignmentContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#forLoop}.
	 * @param ctx the parse tree
	 */
	void enterForLoop(ActonParser.ForLoopContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#forLoop}.
	 * @param ctx the parse tree
	 */
	void exitForLoop(ActonParser.ForLoopContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#forInit}.
	 * @param ctx the parse tree
	 */
	void enterForInit(ActonParser.ForInitContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#forInit}.
	 * @param ctx the parse tree
	 */
	void exitForInit(ActonParser.ForInitContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#forStep}.
	 * @param ctx the parse tree
	 */
	void enterForStep(ActonParser.ForStepContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#forStep}.
	 * @param ctx the parse tree
	 */
	void exitForStep(ActonParser.ForStepContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#ifCondition}.
	 * @param ctx the parse tree
	 */
	void enterIfCondition(ActonParser.IfConditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#ifCondition}.
	 * @param ctx the parse tree
	 */
	void exitIfCondition(ActonParser.IfConditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#multiCondition}.
	 * @param ctx the parse tree
	 */
	void enterMultiCondition(ActonParser.MultiConditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#multiCondition}.
	 * @param ctx the parse tree
	 */
	void exitMultiCondition(ActonParser.MultiConditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#singleCondition}.
	 * @param ctx the parse tree
	 */
	void enterSingleCondition(ActonParser.SingleConditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#singleCondition}.
	 * @param ctx the parse tree
	 */
	void exitSingleCondition(ActonParser.SingleConditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(ActonParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(ActonParser.ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#term}.
	 * @param ctx the parse tree
	 */
	void enterTerm(ActonParser.TermContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#term}.
	 * @param ctx the parse tree
	 */
	void exitTerm(ActonParser.TermContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#factor}.
	 * @param ctx the parse tree
	 */
	void enterFactor(ActonParser.FactorContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#factor}.
	 * @param ctx the parse tree
	 */
	void exitFactor(ActonParser.FactorContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#primary}.
	 * @param ctx the parse tree
	 */
	void enterPrimary(ActonParser.PrimaryContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#primary}.
	 * @param ctx the parse tree
	 */
	void exitPrimary(ActonParser.PrimaryContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#arrElement}.
	 * @param ctx the parse tree
	 */
	void enterArrElement(ActonParser.ArrElementContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#arrElement}.
	 * @param ctx the parse tree
	 */
	void exitArrElement(ActonParser.ArrElementContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#tertiary}.
	 * @param ctx the parse tree
	 */
	void enterTertiary(ActonParser.TertiaryContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#tertiary}.
	 * @param ctx the parse tree
	 */
	void exitTertiary(ActonParser.TertiaryContext ctx);
	/**
	 * Enter a parse tree produced by {@link ActonParser#callMethod}.
	 * @param ctx the parse tree
	 */
	void enterCallMethod(ActonParser.CallMethodContext ctx);
	/**
	 * Exit a parse tree produced by {@link ActonParser#callMethod}.
	 * @param ctx the parse tree
	 */
	void exitCallMethod(ActonParser.CallMethodContext ctx);
}